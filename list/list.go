// Package list implements a persistent, single-linked list.
//
// To iterate over a list (where l is a *List):
//      for i := l; i.Len() > 0; i = i.Tail() {
//			// do something with i.Head()
//      }
package list

// Element is a type for list elements.
type Element interface{}

// node represents single node in the list.
type node struct {
	next  *node
	value Element
}

// List represent persistent, single-linked list.
// The zero value for List is an empty list, ready to use.
type List struct {
	// Value stored in the first element of the list.
	head *node
	// Length of the list.
	length int
}

// New returns an initialized empty list.
func New() List { return *new(List) }

// Push inserts element at the beginning of the list and returns modified list.
func (l List) Push(element Element) List {
	newList := New()
	newList.head = &node{value: element, next: l.head}
	newList.length = l.length + 1
	return newList
}

// Head returns the value stored in the first element of the list, or nil if the list is empty.
func (l List) Head() Element {
	if l.head == nil {
		return nil
	}
	return l.head.value
}

// Tail returns list with the first element removed or an empty list if current list was empty.
func (l List) Tail() List {
	if l.head == nil {
		return New()
	}
	return List{head: l.head.next, length: l.length - 1}
}

// Len returns number of elements in the list.
// The complexity is O(1).
func (l List) Len() int {
	return l.length
}

// Equal check if two lists are equal.
// The complexity is O(n + m), where n and m are lengths of the lists.
func (l List) Equal(other List) bool {
	if l.Len() != other.Len() {
		return false
	}
	for n := l.Len(); n > 0; n-- {
		if l.Head() != other.Head() {
			return false
		}
		l, other = l.Tail(), other.Tail()
	}
	return true
}

// Reverse returns reversed list. The complexity is O(n).
func (l List) Reverse() (reversed List) {
	reversed = New()
	for ; l.Len() > 0; l = l.Tail() {
		reversed = reversed.Push(l.Head())
	}
	return
}

// ToSlice returns elements in the list as a slice.
func (l List) ToSlice() []Element {
	slice := make([]Element, 0, l.Len())
	for ; l.Len() > 0; l = l.Tail() {
		slice = append(slice, l.Head())
	}
	return slice
}
