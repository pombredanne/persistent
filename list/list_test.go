package list

import (
	"fmt"
	"testing"
)

func TestNew(t *testing.T) {
	_ = New()
}

func TestList(t *testing.T) {
	const e1, e2 = 10, 20

	l := New()
	if h := l.Head(); h != nil {
		t.Errorf("l.Head() = %v, want nil", h)
	}
	if ln := l.Len(); ln != 0 {
		t.Errorf("l.Len() = %v, want 0", ln)
	}

	l1 := l.Push(e1)
	if h := l1.Head(); h != e1 {
		t.Errorf("l1.Head() = %v, want %d", h, e1)
	}
	if ln := l1.Len(); ln != 1 {
		t.Errorf("l1.Len() = %v, want 1", ln)
	}

	l2 := l1.Push(e2)
	if h := l2.Head(); h != e2 {
		t.Errorf("l2.Head() = %v, want %d", h, e2)
	}
	if ln := l2.Len(); ln != 2 {
		t.Errorf("l2.Len() = %v, want 2", ln)
	}

	if h := l1.Head(); h != e1 {
		t.Errorf("l1.Head() = %v, want %d", h, e1)
	}
	if ln := l1.Len(); ln != 1 {
		t.Errorf("l1.Len() = %v, want 1", ln)
	}

	l3 := l2.Tail()
	if h := l3.Head(); h != e1 {
		t.Errorf("l3.Head() = %v, want %d", h, e1)
	}
	if ln := l3.Len(); ln != 1 {
		t.Errorf("l3.Len() = %v, want 1", ln)
	}

	if ln := l.Tail().Len(); ln != 0 {
		t.Errorf("l.Tail().Len() = %v, want 0", ln)
	}
}

func TestEqual(t *testing.T) {
	l1 := New().Push(10).Push(5).Push(20)
	l2 := New().Push(10).Push(5).Push(20)
	l3 := New().Push(10).Push(5)
	l4 := New().Push(10).Push(5).Push(25)

	if l1.Equal(l2) != true {
		t.Errorf("l1.Equal(l2) = %v, want true")
	}

	if l1.Equal(l3) != false {
		t.Errorf("l1.Equal(l3) = %v, want false")
	}

	if l1.Equal(l4) != false {
		t.Errorf("l1.Equal(l4) = %v, want false")
	}
}

func TestReverse(t *testing.T) {
	l1 := New().Push(10).Push(5).Push(20)
	l2 := New().Push(20).Push(5).Push(10)

	if r := l1.Reverse(); r.Equal(l2) != true {
		t.Errorf("l1.Reverse() = %v, want %v", r, l2)
	}
}

func TestToSlice(t *testing.T) {
	l := New().Push(10).Push(5).Push(20)
	r := l.ToSlice()
	if len(r) != 3 {
		t.Errorf("ToSlice() length = %v, want 3", len(r))
	}
	for i, e := range []int{20, 5, 10} {
		if e != r[i].(int) {
			t.Errorf("ToSlice() element %v = %v, want %v", i, r[i], e)
		}
	}
}

func ExampleList() {
	l1 := New().Push(10).Push(20).Push(30)
	l2 := l1.Push(40)

	fmt.Printf("l1: ")
	for i := l1; i.Len() > 0; i = i.Tail() {
		fmt.Printf("%v ", i.Head())
	}
	fmt.Printf("   ")

	fmt.Printf("l2: ")
	for i := l2; i.Len() > 0; i = i.Tail() {
		fmt.Printf("%v ", i.Head())
	}
	fmt.Println()

	// Output:
	// l1: 30 20 10    l2: 40 30 20 10
}
