package treemap

import (
	"math/rand"
	"testing"
)

func TestBalancingComparableMap(t *testing.T) {
	items := map[Key]Value{
		"z": 1,
		"y": 2,
		"x": 3,
	}

	m := NewBalancingComparableMap(compareStrings, items)

	if item, err := m.Get("y"); err != nil || item != 2 {
		t.Errorf("Get(\"y\") = (%v, %v), want (2, nil)", item, err)
	}

	if item, err := m.Get("a"); err == nil {
		t.Errorf("Get(\"a\") = (%v, %v), want (_, error)", item, err)
	}
	if item, err := m.Get("zz"); err == nil {
		t.Errorf("Get(\"zz\") = (%v, %v), want (_, error)", item, err)
	}

	mm, err := m.Update("z", 3)
	if err != nil {
		t.Errorf("Update(\"z\", 3) = (%v, %v), want (struct, nil)", mm, err)
	}

	if item, err := mm.Get("z"); err != nil || item != 3 {
		t.Errorf("Get(\"z\") = (%v, %v), want (3, nil)", item, err)
	}

	mm, err = m.Update("a", 3)
	if err != nil {
		t.Errorf("Update(\"a\", 3) = (%v, %v), want (..., nil)", mm, err)
	}

	if item, err := mm.Get("a"); err != nil || item != 3 {
		t.Errorf("Get(\"a\") = (%v, %v), want (3, nil)", item, err)
	}

	m = NewBalancingComparableMap(compareStrings, map[Key]Value{})
	if item, err := m.Get("a"); err == nil {
		t.Errorf("Get(\"a\") = (%v, %v), want (_, error)", item, err)
	}
	m = m.UpdateModifiable("a", 3)
	if item, err := m.Get("a"); err != nil || item != 3 {
		t.Errorf("Get(\"a\") = (%v, %v), want (3, nil)", item, err)
	}
	m = m.Remove("a")
	if item, err := m.Get("a"); err == nil {
		t.Errorf("Get(\"a\") = (%v, %v), want (_, error)", item, err)
	}
	m = m.Remove("a")
	if item, err := m.Get("a"); err == nil {
		t.Errorf("Get(\"a\") = (%v, %v), want (_, error)", item, err)
	}
}

func TestBalancingIntMap(t *testing.T) {
	items := map[int]Value{
		1: "foo",
		5: "bar",
		2: "lol",
	}
	m := NewBalancingIntMap(items)

	if item, err := m.Get(5); item != "bar" || err != nil {
		t.Errorf("Get(5) = (%v, %v), want (\"bar\", nil)", item, err)
	}
	m.Remove(2)
}

func someBalancingIntMap(n int) Map {
	items := map[int]Value{}
	for i := 0; i < n; i++ {
		items[2*i] = 3*i + 10
	}
	return NewBalancingIntMap(items)
}

func BenchmarkBalancingIntMapGet(b *testing.B) {
	n := 100000
	m := someBalancingIntMap(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.Get(rand.Intn(2 * n))
	}
}

func BenchmarkBalancingIntMapUpdateFound(b *testing.B) {
	n := 100000
	m := someBalancingIntMap(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.Update(2*rand.Intn(n), 0)
	}
}

func BenchmarkBalancingIntMapUpdateNotFound(b *testing.B) {
	n := 100000
	m := someBalancingIntMap(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.Update(2*rand.Intn(n)+1, 0)
	}
}
