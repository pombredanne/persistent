package treemap

import (
	"bitbucket.org/sapalskimichal/persistent/tree"
	"bitbucket.org/sapalskimichal/persistent/tree/balancing"
	"fmt"
)

// ModifiableMap is a map to which new items can be added and deleted.
// Note that the Update method never returns an error - if the key doesn't exist, it is added.
type ModifiableMap interface {
	Map
	// Remove returns a new map with given key removed. If the key does not exist, the same map is returned.
	Remove(key Key) ModifiableMap
	// UpdateModifiable is a convenience method - it does the same thing as Update, but returns a ModifiableMap.
	UpdateModifiable(key Key, value Value) ModifiableMap
}

type balancingComparableMap struct {
	compare CompareKeys
	tree    balancing.Tree
}

func (m balancingComparableMap) findByKey(key Key) tree.Finger {
	f := m.tree.Root()
	for {
		d, err := f.Data()
		if err != nil {
			return f
		}
		if f.IsLeaf() {
			c := m.compare(key, d.(keyValue).key)
			r := f
			if c < 0 {
				r, _ = f.Left()
			} else if c > 0 {
				r, _ = f.Right()
			}
			return r
		}
		r, _ := f.Right()
		d, _ = r.Data()
		c := m.compare(key, d.(keyValue).key)
		if c < 0 {
			f, _ = f.Left()
		} else {
			f, _ = f.Right()
		}
	}
}

func (m balancingComparableMap) Get(key Key) (Value, error) {
	f := m.findByKey(key)
	d, err := f.Data()
	if err != nil {
		return nil, fmt.Errorf("Key not found: %v", key)
	}
	return d.(keyValue).value, nil
}

func (m balancingComparableMap) Update(key Key, value Value) (Map, error) {
	return m.UpdateModifiable(key, value), nil
}

func (m balancingComparableMap) UpdateModifiable(key Key, value Value) ModifiableMap {
	f := m.findByKey(key)
	nt := m.tree
	data := keyValue{m.compare, key, value}
	if f.IsLeaf() {
		nt, _ = m.tree.Update(f, data)
	} else {
		nt, _ = m.tree.Insert(f, data)
	}
	return balancingComparableMap{m.compare, nt}
}

func (m balancingComparableMap) Remove(key Key) ModifiableMap {
	f := m.findByKey(key)
	if f.IsLeaf() {
		nt, _ := m.tree.Delete(f)
		return balancingComparableMap{m.compare, nt}
	}
	return m
}

// NewBalancingComparableMap returns a new map with given (key, value) pairs; compare function is used for comparing keys.
func NewBalancingComparableMap(compare CompareKeys, items map[Key]Value) ModifiableMap {
	return balancingComparableMap{compare, balancing.FromSlice(sortKeys(compare, items))}
}

// NewBalancingIntMap returns a new static map with int keys initialized to given (key, value) pairs. Comparison is done by the < operator.
func NewBalancingIntMap(items map[int]Value) ModifiableMap {
	kvItems := make(map[Key]Value)
	for k, v := range items {
		kvItems[k] = v
	}
	return NewBalancingComparableMap(compareInts, kvItems)
}
