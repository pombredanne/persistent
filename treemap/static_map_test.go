package treemap

import (
	"math/rand"
	"testing"
)

func compareStrings(k1, k2 Key) int {
	s1, s2 := k1.(string), k2.(string)
	if s1 == s2 {
		return 0
	} else if s1 < s2 {
		return -1
	} else {
		return 1
	}
}

func TestStaticComparableMap(t *testing.T) {
	items := map[Key]Value{
		"z": 1,
		"y": 2,
		"x": 3,
	}

	m := NewStaticComparableMap(compareStrings, items)

	if item, err := m.Get("y"); err != nil || item != 2 {
		t.Errorf("Get(\"y\") = (%v, %v), want (2, nil)", item, err)
	}

	if item, err := m.Get("a"); err == nil {
		t.Errorf("Get(\"a\") = (%v, %v), want (_, error)", item, err)
	}
	if item, err := m.Get("zz"); err == nil {
		t.Errorf("Get(\"zz\") = (%v, %v), want (_, error)", item, err)
	}

	mm, err := m.Update("z", 3)
	if err != nil {
		t.Errorf("Update(\"z\", 3) = (%v, %v), want (struct, nil)", mm, err)
	}

	if item, err := mm.Get("z"); err != nil || item != 3 {
		t.Errorf("Get(\"z\") = (%v, %v), want (3, nil)", item, err)
	}

	mm, err = m.Update("a", 3)
	if err == nil {
		t.Errorf("Update(\"a\", 3) = (%v, %v), want (_, error)", mm, err)
	}

	m = NewStaticComparableMap(compareStrings, map[Key]Value{})
	if item, err := m.Get("a"); err == nil {
		t.Errorf("Get(\"a\") = (%v, %v), want (_, error)", item, err)
	}

}

func TestStaticIntMap(t *testing.T) {
	items := map[int]Value{
		1: "foo",
		5: "bar",
		2: "lol",
	}
	m := NewStaticIntMap(items)

	if item, err := m.Get(5); item != "bar" || err != nil {
		t.Errorf("Get(5) = (%v, %v), want (\"bar\", nil)", item, err)
	}
}

func someStaticIntMap(n int) Map {
	items := map[int]Value{}
	for i := 0; i < n; i++ {
		items[2*i] = 3*i + 10
	}
	return NewStaticIntMap(items)
}

func BenchmarkStaticIntMapGet(b *testing.B) {
	n := 100000
	m := someStaticIntMap(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.Get(rand.Intn(2 * n))
	}
}

func BenchmarkStaticIntMapUpdate(b *testing.B) {
	n := 100000
	m := someStaticIntMap(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.Update(rand.Intn(2*n), 0)
	}
}

func TestStaticStringMap(t *testing.T) {
	items := map[string]Value{
		"one":  "foo",
		"five": "bar",
		"two":  "lol",
	}
	m := NewStaticStringMap(items)

	if item, err := m.Get("five"); item != "bar" || err != nil {
		t.Errorf("Get(\"five\") = (%v, %v), want (\"bar\", nil)", item, err)
	}
}

func TestSummingComparableMap(t *testing.T) {
	items := map[Key]int{
		"a": 100000,
		"c": 20000,
		"e": 3000,
		"g": 400,
		"i": 50,
		"k": 6,
	}
	m := NewStaticComparableSummingMap(compareStrings, items)

	if item, err := m.Get("c"); err != nil || item != 20000 {
		t.Errorf("Get(\"c\") = (%v, %v), want (20000, nil)", item, err)
	}

	mm, err := m.Update("c", 2)
	if err != nil {
		t.Errorf("Update(\"c\", 2) = (%v, %v), want (..., nil)", mm, err)
	}
	if item, err := mm.Get("c"); err != nil || item != 2 {
		t.Errorf("Get(\"c\") = (%v, %v), want (2, nil)", item, err)
	}
	mm2, err := m.UpdateSumming("c", 2)
	if err != nil {
		t.Errorf("Update(\"c\", 2) = (%v, %v), want (..., nil)", mm, err)
	}
	if sum := mm2.Sum("c", "c"); sum != 2 {
		t.Errorf("Sum(\"c\", \"c\") = %v, want 2", sum)
	}

	if sum := m.Sum("", ""); sum != 0 {
		t.Errorf("Sum(\"\", \"\") = %v, want 0", sum)
	}

	if sum := m.Sum("c", "i"); sum != 23450 {
		t.Errorf("Sum(\"c\", \"i\") = %v, want 23450", sum)
	}

	if sum := m.Sum("b", "i"); sum != 23450 {
		t.Errorf("Sum(\"b\", \"i\") = %v, want 23450", sum)
	}

	if sum := m.Sum("b", "j"); sum != 23450 {
		t.Errorf("Sum(\"b\", \"j\") = %v, want 23450", sum)
	}

	if sum := m.Sum("a", "a"); sum != 100000 {
		t.Errorf("Sum(\"a\", \"a\") = %v, want 100000", sum)
	}

	if sum := m.Sum("g", "a"); sum != 123400 {
		t.Errorf("Sum(\"g\", \"a\") = %v, want 123400", sum)
	}

	if sum := m.Sum("t", "z"); sum != 0 {
		t.Errorf("Sum(\"t\", \"z\") = %v, want 0", sum)
	}

	m = NewStaticComparableSummingMap(compareStrings, map[Key]int{"b": 10})
	if sum := m.Sum("a", "z"); sum != 10 {
		t.Errorf("Sum(\"a\", \"z\") = %v, want 10", sum)
	}

	m = NewStaticComparableSummingMap(compareStrings, map[Key]int{})
	if item, err := m.Get("c"); err == nil {
		t.Errorf("Get(\"c\") = (%v, %v), want (*, error)", item, err)
	}
	if mm, err := m.UpdateSumming("c", 10); err == nil {
		t.Errorf("UpdateSumming(\"c\", 10) = (%v, %v), want (*, error)", mm, err)
	}
	if sum := m.Sum("t", "z"); sum != 0 {
		t.Errorf("Sum(\"t\", \"z\") = %v, want 0", sum)
	}
}

func same(result, expected []Key) bool {
	if len(result) != len(expected) {
		return false
	}
	for i, v := range result {
		if expected[i] != v {
			return false
		}
	}
	return true
}

func TestNonZeroEntries(t *testing.T) {
	items := map[Key]int{
		"a": 1,
		"c": 0,
		"e": 1,
		"g": 1,
		"i": 0,
		"k": 6,
	}
	m := NewStaticComparableSummingMap(compareStrings, items)
	if result := m.NonZeroEntries("c", "h", -1); !same(result, []Key{"e", "g"}) {
		t.Errorf("NonZeroEntries(\"c\", \"h\", -1) = %v, want [\"e\", \"g\"]", result)
	}

	if result := m.NonZeroEntries("c", "h", 1); !same(result, []Key{"e"}) {
		t.Errorf("NonZeroEntries(\"c\", \"h\", 1) = %v, want [\"e\"]", result)
	}

	if result := m.NonZeroEntries("k", "g", -1); !same(result, []Key{}) {
		t.Errorf("NonZeroEntries(\"k\", \"g\", -1) = %v, want []", result)
	}

	m = NewStaticComparableSummingMap(compareStrings, map[Key]int{})
	if result := m.NonZeroEntries("a", "z", -1); !same(result, []Key{}) {
		t.Errorf("NonZeroEntries(\"a\", \"z\", -1) = %v, want []", result)
	}
}

func TestStaticIntSummingMap(t *testing.T) {
	items := map[int]int{
		1: 10,
		5: 50,
		2: 20,
	}
	m := NewStaticIntSummingMap(items)

	if sum := m.Sum(1, 5); sum != 80 {
		t.Errorf("Sum(1, 5) = %v, want 80", sum)
	}
}

func TestStaticStringSummingMap(t *testing.T) {
	items := map[string]int{
		"a": 10,
		"x": 50,
		"d": 20,
	}
	m := NewStaticStringSummingMap(items)

	if sum := m.Sum("a", "z"); sum != 80 {
		t.Errorf("Sum(\"a\", \"z\") = %v, want 80", sum)
	}
}

func TestFindByPrefixSum(t *testing.T) {
	items := map[string]int{
		"a": 10,
		"x": 50,
		"d": 20,
	}
	m := NewStaticStringSummingMap(items)

	if res, err := m.FindByPrefixSum(15); err != nil || res.(string) != "d" {
		t.Errorf("FindByPrefixSum(15) = (%v, %v), want (\"d\", nil)", res, err)
	}

	if res, err := m.FindByPrefixSum(0); err != nil || res.(string) != "a" {
		t.Errorf("FindByPrefixSum(0) = (%v, %v), want (\"a\", nil)", res, err)
	}

	if res, err := m.FindByPrefixSum(100); err == nil {
		t.Errorf("FindByPrefixSum(100) = (%v, %v), want (*, error)", res, err)
	}
}

func TestSubtract(t *testing.T) {
	itemsA := map[string]int{
		"a": 110,
		"x": 550,
		"d": 220,
	}
	itemsB := map[string]int{
		"a": 10,
		"x": 50,
		"d": 20,
	}

	m := Subtract(NewStaticStringSummingMap(itemsA), NewStaticStringSummingMap(itemsB))

	if res, err := m.Get("x"); err != nil || res != 500 {
		t.Errorf("Get(\"x\") = (%v, %v), want (500, nil)", res, err)
	}
	if res := m.Sum("a", "x"); res != 800 {
		t.Errorf("Sum(\"a\", \"x\") = %v, want 800", res)
	}
}

func TestAdd(t *testing.T) {
	itemsA := map[string]int{
		"a": 100,
		"x": 500,
		"d": 200,
	}
	itemsB := map[string]int{
		"a": 10,
		"x": 50,
		"d": 20,
	}

	m := Add(NewStaticStringSummingMap(itemsA), NewStaticStringSummingMap(itemsB))

	if res, err := m.Get("x"); err != nil || res != 550 {
		t.Errorf("Get(\"x\") = (%v, %v), want (550, nil)", res, err)
	}
	if res := m.Sum("a", "x"); res != 880 {
		t.Errorf("Sum(\"a\", \"x\") = %v, want 880", res)
	}
}
