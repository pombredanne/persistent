// Package treemap implements persistent map using a binary search tree.
package treemap

import (
	"bitbucket.org/sapalskimichal/persistent/tree"
	"bitbucket.org/sapalskimichal/persistent/tree/static"
	"fmt"
	"sort"
)

// Key is the type for keys in the map.
type Key interface{}

// Value is the type for values in the map.
type Value interface{}

// CompareKeys is a type of a function that can be used to compare keys.
// It should return 0 if the keys are equal, a negative number if the first key is less than the second or positive number otherwise.
type CompareKeys func(k1, k2 Key) int

// Map is a specification of a persistent map data structure.
type Map interface {
	// Get returns the value associated with the given key, or error if it cannot be found.
	Get(key Key) (Value, error)
	// Update returns new version of the structure, in which given key has a given value, or error if this value cannot be updated.
	Update(key Key, value Value) (Map, error)
}

// SumingMap is a specification of a persistent summing map data structure: in addition to normal map operations, it supports taking sum of the values for a certain range of keys.
type SummingMap interface {
	Map
	// Sum returns the sum of all values between key1 and key2 (inclusive).
	Sum(key1, key2 Key) int
	// UpdateSumming is a conveinience method -- it does the same thing as Update, but returns a SummingMap
	UpdateSumming(key Key, value int) (SummingMap, error)
	// NonZeroEntries returns a slice containing keys with non-zero values between key1 and key2 (inclusive) sorted in ascending order. If 'limit' is a non-negative value, at most 'limit' keys will be returned.
	NonZeroEntries(key1, key2 Key, limit int) []Key
	// FindByPrefixSum returns the first key k such that the sum of values of keys <= k is at least s, or error if such key doesn't exist. This function works only if all values in the map are non-negative.
	FindByPrefixSum(s int) (Key, error)
}

type minKey interface {
	MinKey() Key
}

type keyValue struct {
	compare CompareKeys
	key     Key
	value   Value
}

func (kv keyValue) CombineWith(_ tree.Data) tree.Data {
	return kv
}

func (kv keyValue) MinKey() Key {
	return kv.key
}

type summingKeyValue struct {
	compare CompareKeys
	key     Key
	maxKey  Key
	value   int
	nonZero bool
}

func (kv summingKeyValue) CombineWith(other tree.Data) tree.Data {
	o := other.(summingKeyValue)
	return summingKeyValue{kv.compare, kv.key, o.maxKey, kv.value + o.value, kv.nonZero || o.nonZero}
}

func (kv summingKeyValue) MinKey() Key {
	return kv.key
}

type staticComparableMap struct {
	compare CompareKeys
	tree    static.Tree
}

type staticComparableSummingMap struct {
	compare CompareKeys
	tree    static.Tree
}

type byCompare struct {
	compare CompareKeys
	data    []tree.Data
}

func (b byCompare) Len() int {
	return len(b.data)
}

func (b byCompare) Swap(i, j int) {
	b.data[i], b.data[j] = b.data[j], b.data[i]
}

func (b byCompare) Less(i, j int) bool {
	return b.compare(b.data[i].(minKey).MinKey(), b.data[j].(minKey).MinKey()) < 0
}

type mode int

const (
	modeRight = iota
	modeExact
)

func find(tree tree.Tree, compare CompareKeys, key Key, mode mode) (tree.Finger, error) {
	f := tree.Root()
	data, err := f.Data()
	if err != nil {
		return nil, err
	}
	if mode == modeExact && compare(key, data.(minKey).MinKey()) < 0 {
		return nil, fmt.Errorf("Key not found: %v", key)
	}
	for {
		if f.IsLeaf() {
			data, _ := f.Data()
			if mode == modeRight || 0 == compare(data.(minKey).MinKey(), key) {
				return f, nil
			}
			return nil, fmt.Errorf("Key not found: %v", key)
		}
		right, _ := f.Right()
		data, _ := right.Data()
		c := compare(data.(minKey).MinKey(), key)
		if c <= 0 {
			f = right
		} else {
			f, _ = f.Left()
		}
	}
}

func (m staticComparableMap) Get(key Key) (Value, error) {
	f, err := find(m.tree, m.compare, key, modeExact)
	if err != nil {
		return nil, err
	} else {
		data, _ := f.Data()
		return data.(keyValue).value, nil
	}
}

func (m staticComparableMap) Update(key Key, value Value) (Map, error) {
	f, err := find(m.tree, m.compare, key, modeExact)
	if err != nil {
		return nil, err
	}
	t, err := m.tree.Update(f, keyValue{m.compare, key, value})
	return staticComparableMap{m.compare, t}, nil
}

func (m staticComparableSummingMap) Get(key Key) (Value, error) {
	f, err := find(m.tree, m.compare, key, modeExact)
	if err != nil {
		return nil, err
	} else {
		data, _ := f.Data()
		return data.(summingKeyValue).value, nil
	}
}

func (m staticComparableSummingMap) Sum(key1, key2 Key) int {
	f1, err := find(m.tree, m.compare, key1, modeRight)
	if err != nil {
		return 0
	}
	f2, _ := find(m.tree, m.compare, key2, modeRight)
	s, _ := static.CombineBetween(f1, f2)
	result := s.(summingKeyValue).value
	data, _ := f1.Data()
	kv := data.(summingKeyValue)
	if m.compare(kv.key, key1) < 0 {
		result -= kv.value
	}
	data, _ = f2.Data()
	kv = data.(summingKeyValue)
	if m.compare(key2, kv.key) < 0 {
		result -= kv.value
	}
	return result
}

func (m staticComparableSummingMap) Update(key Key, value Value) (Map, error) {
	f, err := find(m.tree, m.compare, key, modeExact)
	if err != nil {
		return nil, err
	}
	t, err := m.tree.Update(f, summingKeyValue{m.compare, key, key, value.(int), value.(int) != 0})
	return staticComparableSummingMap{m.compare, t}, nil
}

func (m staticComparableSummingMap) UpdateSumming(key Key, value int) (SummingMap, error) {
	mm, err := m.Update(key, value)
	if err != nil {
		return nil, err
	}
	return mm.(SummingMap), nil
}

func nonZeroEntries(f tree.Finger, compare CompareKeys, key1, key2 Key, result []Key, limit int) []Key {
	data, err := f.Data()
	if err != nil || limit == 0 {
		return result
	}
	d := data.(summingKeyValue)

	if !d.nonZero || compare(key2, d.key) < 0 || compare(d.maxKey, key1) < 0 {
		return result
	}
	if f.IsLeaf() {
		k := d.key
		return append(result, k)
	}
	left, _ := f.Left()
	prevLen := len(result)
	result = nonZeroEntries(left, compare, key1, key2, result, limit)
	if limit >= 0 {
		limit -= (len(result) - prevLen)
	}
	right, _ := f.Right()
	result = nonZeroEntries(right, compare, key1, key2, result, limit)
	return result
}
func (m staticComparableSummingMap) NonZeroEntries(key1, key2 Key, limit int) []Key {
	result := nonZeroEntries(m.tree.Root(), m.compare, key1, key2, make([]Key, 0, 8), limit)
	return result
}

func (m staticComparableSummingMap) FindByPrefixSum(s int) (Key, error) {
	f := m.tree.Root()
	data, err := f.Data()
	if err != nil || data.(summingKeyValue).value < s {
		return nil, fmt.Errorf("No key with prefix sum %v", s)
	}
	for !f.IsLeaf() {
		left, _ := f.Left()
		data, _ := left.Data()
		leftSum := data.(summingKeyValue).value
		if leftSum >= s {
			f = left
		} else {
			s -= leftSum
			f, _ = f.Right()
		}
	}
	data, err = f.Data()
	return data.(summingKeyValue).key, nil
}

func sortKeys(compare CompareKeys, items map[Key]Value) []tree.Data {
	cmp := byCompare{compare, make([]tree.Data, 0, len(items))}
	for k, v := range items {
		cmp.data = append(cmp.data, keyValue{compare, k, v})
	}
	sort.Sort(cmp)
	return cmp.data
}

// NewStaticComparableMap returns new map containing given (key, value) pairs, compare function is used for comparing keys. The map is static, which means new keys cannot be added (but existing ones can be updated).
func NewStaticComparableMap(compare CompareKeys, items map[Key]Value) Map {
	return staticComparableMap{compare, static.FromSlice(sortKeys(compare, items))}
}

func compareInts(k1, k2 Key) int {
	return k1.(int) - k2.(int)
}

// NewStaticIntMap returns new static map with int keys initialized to given (key, value) pairs. Comparison is done by the < operator.
func NewStaticIntMap(items map[int]Value) Map {
	kvItems := make(map[Key]Value)
	for k, v := range items {
		kvItems[k] = v
	}
	return NewStaticComparableMap(compareInts, kvItems)
}

// NewStaticStringMap returns new static map with string keys initialized to given (key, value) pairs. Comparison is done by the < operator.
func NewStaticStringMap(items map[string]Value) Map {
	compare := func(k1, k2 Key) int {
		s1, s2 := k1.(string), k2.(string)
		if s1 == s2 {
			return 0
		} else if s1 < s2 {
			return -1
		} else {
			return 1
		}
	}
	kvItems := make(map[Key]Value)
	for k, v := range items {
		kvItems[k] = v
	}
	return NewStaticComparableMap(compare, kvItems)
}

// NewStaticComparableSummingMap returns new summing map containing given (key, value) pairs, compare function is used for comparing keys. The map is static, which means new keys cannot be added (but existing ones can be updated).
func NewStaticComparableSummingMap(compare CompareKeys, items map[Key]int) SummingMap {
	cmp := byCompare{compare, make([]tree.Data, 0, len(items))}
	for k, v := range items {
		cmp.data = append(cmp.data, summingKeyValue{compare, k, k, v, v != 0})
	}
	sort.Sort(cmp)
	return staticComparableSummingMap{compare, static.FromSlice(cmp.data)}
}

// NewStaticIntSummingMap returns new static summing map with int keys initialized to given (key, value) pairs. Comparison is done by the < operator.
func NewStaticIntSummingMap(items map[int]int) SummingMap {
	compare := func(k1, k2 Key) int {
		return k1.(int) - k2.(int)
	}
	kvItems := make(map[Key]int)
	for k, v := range items {
		kvItems[k] = v
	}
	return NewStaticComparableSummingMap(compare, kvItems)
}

// NewStaticStringSummingMap returns new static summing map with string keys initialized to given (key, value) pairs. Comparison is done by the < operator.
func NewStaticStringSummingMap(items map[string]int) SummingMap {
	compare := func(k1, k2 Key) int {
		s1, s2 := k1.(string), k2.(string)
		if s1 < s2 {
			return -1
		} else if s1 == s2 {
			return 0
		} else {
			return 1
		}
	}
	kvItems := make(map[Key]int)
	for k, v := range items {
		kvItems[k] = v
	}
	return NewStaticComparableSummingMap(compare, kvItems)
}

func subtractSummingKeyValue(d1, d2 tree.Data) tree.Data {
	kv1, kv2 := d1.(summingKeyValue), d2.(summingKeyValue)
	return summingKeyValue{kv1.compare, kv1.key, kv1.maxKey, kv1.value - kv2.value, kv1.nonZero}
}

// Subtract returns a new summing map where value for key k equals a[k] - b[k]. The map is constructed on-the-fly as new nodes are accessed. Note that the maps a and b must contain exactly the same elements and have the same comparator. The NonZeroEntries method of newly constructed map may return wrong results.
func Subtract(a, b SummingMap) SummingMap {
	am, bm := a.(staticComparableSummingMap), b.(staticComparableSummingMap)
	return staticComparableSummingMap{am.compare, static.CombineTrees(subtractSummingKeyValue, am.tree, bm.tree)}
}

func addSummingKeyValue(d1, d2 tree.Data) tree.Data {
	kv1, kv2 := d1.(summingKeyValue), d2.(summingKeyValue)
	return summingKeyValue{kv1.compare, kv1.key, kv1.maxKey, kv1.value + kv2.value, kv1.nonZero}
}

// Add returns a new summing map where value for key k equals a[k] + b[k]. The map is constructed on-the-fly as new nodes are accessed. Note that the maps a and b must contain exactly the same elements and have the same comparator. The NonZeroEntries method of newly constructed map may return wrong results.
func Add(a, b SummingMap) SummingMap {
	am, bm := a.(staticComparableSummingMap), b.(staticComparableSummingMap)
	return staticComparableSummingMap{am.compare, static.CombineTrees(addSummingKeyValue, am.tree, bm.tree)}
}
