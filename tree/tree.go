// tree package defines the interface for a binary tree which stores data in its leaves and uses branch nodes for navigation.
package tree

// Data interface is used to store data in the tree.
type Data interface {
	// CombineWith combines this piece of data with other piece of data to provide a summary of both.
	// The tree should store only types of data that can be safely combined with each other (preferably only one type of data).
	CombineWith(other Data) Data
}

// Tree is an interface that represents a binary tree.
type Tree interface {
	// Root provides a finger pointing at the root element of the tree. If the tree is empty, the finger returned will point to nil node.
	Root() Finger
}

// Finger is a pointer to a specific node in a tree. It can be used for navigating around the tree.
type Finger interface {
	// Data returns the data stored in node pointed by this finger or error if this finger points to nil node.
	Data() (Data, error)
	// Left returns a finger to the left child of this node or finger pointing to a nil node if this node is a leaf. If this node is a nil node, Left returns an error.
	Left() (Finger, error)
	// Right returns a finger to the right child of this node or finger pointing to a nil node if this node is a leaf. If this node is a nil node, Right returns an error.
	Right() (Finger, error)
	// Up returns a finger pointing to the parent of this node or returns an error if it's a root node.
	Up() (Finger, error)
	// IsLeaf returns true if this finger points to a leaf.
	IsLeaf() bool
	// IsLeft returns true is this finger points to a left child of its parent.
	IsLeft() bool
	// IsRight returns true is this finger points to a right child of its parent.
	IsRight() bool
}
