package static

import (
	"bitbucket.org/sapalskimichal/persistent/tree"
	"testing"
)

type IntSum int

func (data IntSum) CombineWith(other tree.Data) tree.Data {
	return IntSum(data + other.(IntSum))
}

func TestFromSlice(t *testing.T) {
	tree := FromSlice([]tree.Data{IntSum(3), IntSum(2), IntSum(4), IntSum(5), IntSum(6)})
	root := tree.Root()
	if data, err := root.Data(); err != nil || data.(IntSum) != 20 {
		t.Errorf("Root().Data() = (%v, %v), want (20, nil)", data, err)
	}

	if up, err := root.Up(); err == nil {
		t.Errorf("Root().Up() = (%v, %v), want (*, error)", up, err)
	}

	if root.IsLeft() {
		t.Errorf("root.IsLeft() = true, want false")
	}
	if root.IsRight() {
		t.Errorf("root.IsRight() = true, want false")
	}

	left, err := root.Left()
	if err != nil {
		t.Errorf("Left() = (%v, %v), want (..., nil)", left, err)
	}
	if data, err := left.Data(); err != nil || data.(IntSum) != 5 {
		t.Errorf("Left().Data() = (%v, %v), want (5, nil)", data, err)
	}
	if !left.IsLeft() {
		t.Errorf("left.IsLeft() = false, want true")
	}
	if left.IsRight() {
		t.Errorf("left.IsRight() = true, want false")
	}

	right, err := root.Right()
	if err != nil {
		t.Errorf("Right() = (%v, %v), want (..., nil)", right, err)
	}
	if data, err := right.Data(); err != nil || data.(IntSum) != 15 {
		t.Errorf("Right().Data() = (%v, %v), want (5, nil)", data, err)
	}
	if right.IsLeft() {
		t.Errorf("right.IsLeft() = true, want false")
	}
	if !right.IsRight() {
		t.Errorf("right.IsRight() = false, want true")
	}

	root, err = left.Up()
	if err != nil {
		t.Errorf("Up() = (%v, %v), want (root, nil)", left, err)
	}
	if data, err := root.Data(); err != nil || data.(IntSum) != 20 {
		t.Errorf("Root().Data() = (%v, %v), want (20, nil)", data, err)
	}

	left, err = left.Left()
	if err != nil {
		t.Errorf("Left() = (%v, %v), want (..., nil)", left, err)
	}
	if !left.IsLeaf() {
		t.Errorf("IsLeaf() = false, want true")
	}

	left, err = left.Left()
	if err != nil {
		t.Errorf("Left() = (%v, %v), want (..., nil)", left, err)
	}
	if data, err := left.Data(); err == nil {
		t.Errorf("Left().Data() = (%v, %v), want (*, error)", data, err)
	}
	if l, err := left.Left(); err == nil {
		t.Errorf("Left() = (%v, %v), want (*, error)", l, err)
	}
	if r, err := left.Right(); err == nil {
		t.Errorf("Right() = (%v, %v), want (*, error)", r, err)
	}

}

func TestEmptyTree(t *testing.T) {
	tree := FromSlice([]tree.Data{})
	root := tree.Root()
	if data, err := root.Data(); err == nil {
		t.Errorf("Root().Data() = (%v, %v), want (*, error)", data, err)
	}
	if root.IsLeaf() {
		t.Errorf("Root().IsLeaf() = true, want false")
	}
}

func TestUpdate(t *testing.T) {
	tree := FromSlice([]tree.Data{IntSum(1), IntSum(2), IntSum(3)})
	node := tree.Root()
	node, _ = node.Right()
	if tree, err := tree.Update(node, IntSum(4)); err == nil {
		t.Errorf("Update(4) = (%v, %v), want (..., error)", tree, err)
	}
	node, _ = node.Left()
	newTree, err := tree.Update(node, IntSum(4))
	if err != nil {
		t.Errorf("Update(4) = (%v, %v), want (..., nil)", newTree, err)
	}
	root := newTree.Root()
	if data, err := root.Data(); err != nil || data.(IntSum) != 8 {
		t.Errorf("Root().Data() = (%v, %v), want (4, nil)", data, err)
	}

	node, _ = node.Left()
	if newTree, err := tree.Update(node, IntSum(4)); err == nil {
		t.Errorf("Update(4) = (%v, %v), want (..., error)", newTree, err)
	}
}

func TestCombineBetween(t *testing.T) {
	tree := FromSlice([]tree.Data{IntSum(10000000), IntSum(2000000), IntSum(300000), IntSum(40000), IntSum(5000), IntSum(600), IntSum(70), IntSum(8)})

	root := tree.Root()
	f1, _ := root.Left()
	f2, _ := root.Right()
	f2, _ = f2.Left()
	if value, err := CombineBetween(f1, f2); err != nil || value.(IntSum) != 12345600 {
		t.Errorf("CombineBetween(f1, f2) = (%v, %v), want (12345600, nil)", value, err)
	}

	f1, _ = f1.Left()
	f1, _ = f1.Right()
	f2, _ = f2.Right()
	if value, err := CombineBetween(f1, f2); err != nil || value.(IntSum) != 2345600 {
		t.Errorf("CombineBetween(f1, f2) = (%v, %v), want (2345000, nil)", value, err)
	}
	if value, err := CombineBetween(f2, f1); err != nil || value.(IntSum) != 2345600 {
		t.Errorf("CombineBetween(f2, f1) = (%v, %v), want (2345000, nil)", value, err)
	}

	if value, err := CombineBetween(f1, f1); err != nil || value.(IntSum) != 2000000 {
		t.Errorf("CombineBetween(f1, f1) = (%v, %v), want (2000000, nil)", value, err)
	}

	nilNode, _ := f1.Left()
	if value, err := CombineBetween(nilNode, f1); err == nil {
		t.Errorf("CombineBetween(nilNode, f1) = (%v, %v), want (*, error)", value, err)
	}
	if value, err := CombineBetween(f1, nilNode); err == nil {
		t.Errorf("CombineBetween(f1, nilNode) = (%v, %v), want (*, error)", value, err)
	}

	if value, err := CombineBetween(f1, tree.Root()); err != nil || value.(IntSum) != 12345678 {
		t.Errorf("CombineBetween(f1, root) = (%v, %v), want (12345678, nil)", value, err)
	}
}

func TestCombineBetweenOnCombinedTrees(t *testing.T) {
	treeA := FromSlice([]tree.Data{IntSum(10000000), IntSum(2000000), IntSum(300000), IntSum(40000)})
	treeB := FromSlice([]tree.Data{IntSum(1000), IntSum(200), IntSum(30), IntSum(4)})

	combinator := func(dA, dB tree.Data) tree.Data {
		return IntSum(dA.(IntSum) + dB.(IntSum))
	}
	tree := CombineTrees(combinator, treeA, treeB)

	r := tree.Root()
	left, _ := r.Left()
	left, _ = left.Right()
	right, _ := r.Right()
	right, _ = right.Left()

	if value, err := CombineBetween(left, right); err != nil || value.(IntSum) != 2300230 {
		t.Errorf("CombineBetween(left, right) = (%v, err), want (2300230, nil)", value, err)
	}
}

func TestCombineTrees(t *testing.T) {
	treeA := FromSlice([]tree.Data{IntSum(10000000), IntSum(2000000), IntSum(300000), IntSum(40000), IntSum(5000), IntSum(600), IntSum(70), IntSum(8)})
	treeB := FromSlice([]tree.Data{IntSum(100000000), IntSum(100000000), IntSum(100000000), IntSum(100000000), IntSum(100000000), IntSum(100000000), IntSum(100000000), IntSum(100000000)})

	combinator := func(dA, dB tree.Data) tree.Data {
		return IntSum(dA.(IntSum) + dB.(IntSum))
	}

	tree := CombineTrees(combinator, treeA, treeB)

	root := tree.Root()
	if data, err := root.Data(); err != nil || data.(IntSum) != 812345678 {
		t.Errorf("Root().Data() = (%v, %v), want (812345678, nil)", data, err)
	}
	if root.IsLeft() {
		t.Errorf("root.IsLeft() = true, want false")
	}
	if root.IsRight() {
		t.Errorf("root.IsRight() = true, want false")
	}

	left, err := root.Left()
	if err != nil {
		t.Errorf("Left() = (%v, %v), want (..., nil)", left, err)
	}
	if data, err := left.Data(); err != nil || data.(IntSum) != 412340000 {
		t.Errorf("Left().Data() = (%v, %v), want (412340000, nil)", data, err)
	}
	if !left.IsLeft() {
		t.Errorf("left.IsLeft() = false, want true")
	}
	if left.IsRight() {
		t.Errorf("left.IsRight() = true, want false")
	}

	right, err := root.Right()
	if err != nil {
		t.Errorf("Right() = (%v, %v), want (..., nil)", right, err)
	}
	if data, err := right.Data(); err != nil || data.(IntSum) != 400005678 {
		t.Errorf("Right().Data() = (%v, %v), want (400005678, nil)", data, err)
	}
	if right.IsLeft() {
		t.Errorf("right.IsLeft() = true, want false")
	}
	if !right.IsRight() {
		t.Errorf("right.IsRight() = false, want true")
	}

	leaf, _ := left.Left()
	leaf, _ = leaf.Left()
	if left.IsLeaf() {
		t.Errorf("left.IsLeaf() = true, want false")
	}
	if !leaf.IsLeaf() {
		t.Errorf("leaf.IsLeaf() = false, want true")
	}

	root, err = left.Up()
	if err != nil {
		t.Errorf("left.Up() = (%v, %v), want (root, nil)", root, err)
	}
	if data, err := root.Data(); err != nil || data.(IntSum) != 812345678 {
		t.Errorf("Root().Data() = (%v, %v), want (812345678, nil)", data, err)
	}

	nt, err := tree.Update(leaf, IntSum(0))
	if err == nil {
		t.Errorf("leaf.Update(0) = (%v, %v), want (..., error)", nt, err)
	}

	nilNode, _ := leaf.Left()
	if node, err := nilNode.Left(); err == nil {
		t.Errorf("nilNode.Left() = (%v, %v), want (*, error)", node, err)
	}
	if node, err := nilNode.Right(); err == nil {
		t.Errorf("nilNode.Right() = (%v, %v), want (*, error)", node, err)
	}
	if data, err := nilNode.Data(); err == nil {
		t.Errorf("nilNode.Data() = (%v, %v), want (*, error)", data, err)
	}
}
