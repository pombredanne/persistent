// static package implements static binary tree.
// New nodes cannot be added to this kind of tree after it has been created, but old nodes can be updated.
// The values are stored in leaves of the tree while branch nodes provide a summary of values stored in its subtrees.
package static

import (
	"bitbucket.org/sapalskimichal/persistent/list"
	"bitbucket.org/sapalskimichal/persistent/tree"
	"errors"
)

// Tree is an interface for static tree. Static trees do not change their shape once they have been created, however their nodes can be updated.
type Tree interface {
	tree.Tree
	// Update updates current node and returns a new tree. Only leaf nodes can be updated - if Update is called on branch node or nil node, it returns an error.
	Update(f tree.Finger, newData tree.Data) (Tree, error)
}

type node struct {
	data  tree.Data
	left  *node
	right *node
}

type staticTree struct {
	root *node
}

type finger struct {
	path list.List
}

func (f finger) Data() (tree.Data, error) {
	n := f.path.Head().(*node)
	if n != nil {
		return n.data, nil
	}
	return nil, errors.New("No such node")
}

func (f finger) down(left bool) (tree.Finger, error) {
	n := f.path.Head().(*node)
	if n == nil {
		return nil, errors.New("No such node")
	}
	if left {
		return finger{f.path.Push(n.left)}, nil
	} else {
		return finger{f.path.Push(n.right)}, nil
	}
}

func (f finger) Left() (tree.Finger, error) {
	return f.down(true)
}

func (f finger) Right() (tree.Finger, error) {
	return f.down(false)
}

func (f finger) Up() (tree.Finger, error) {
	if f.path.Len() > 1 {
		return finger{f.path.Tail()}, nil
	}
	return nil, errors.New("Cannot go up from root")
}

func (f finger) IsLeaf() bool {
	n := f.path.Head().(*node)
	return n != nil && n.left == nil
}

func (f finger) IsLeft() bool {
	p, err := f.Up()
	if err != nil {
		return false
	}
	return p.(finger).path.Head().(*node).left == f.path.Head().(*node)
}

func (f finger) IsRight() bool {
	p, err := f.Up()
	if err != nil {
		return false
	}
	return p.(finger).path.Head().(*node).right == f.path.Head().(*node)
}

func combine(left, right *node) *node {
	return &node{left.data.CombineWith(right.data), left, right}
}

func (t staticTree) Update(f tree.Finger, newData tree.Data) (Tree, error) {
	ff := f.(finger)
	n := ff.path.Head().(*node)
	if n == nil || n.left != nil {
		return nil, errors.New("Cannot update this node")
	}
	nd := &node{newData, nil, nil}
	path := ff.path
	for path.Len() > 1 {
		n := path.Head().(*node)
		path = path.Tail()
		p := path.Head().(*node)
		if p.left == n {
			nd = combine(nd, p.right)
		} else {
			nd = combine(p.left, nd)
		}
	}
	return staticTree{nd}, nil
}

func (t staticTree) Root() tree.Finger {
	return finger{list.New().Push(t.root)}
}

func nodeFromSlice(data []tree.Data) *node {
	n := len(data)
	if n == 0 {
		return nil
	}
	if n == 1 {
		return &node{data[0], nil, nil}
	}
	return combine(nodeFromSlice(data[:n/2]), nodeFromSlice(data[n/2:]))
}

// FromSlice creates a static tree which contains elements from given slice in its leaves.
func FromSlice(data []tree.Data) Tree {
	return staticTree{nodeFromSlice(data)}
}

func toSlice(f tree.Finger) []tree.Finger {
	s := append([]tree.Finger{f})
	for p, err := f.Up(); err == nil; p, err = f.Up() {
		s = append(s, p)
		f = p
	}
	r := make([]tree.Finger, len(s))
	for i, v := range s {
		r[len(s)-1-i] = v
	}
	return r
}

func combineRight(path []tree.Finger, idx int) tree.Data {
	if idx == len(path)-1 {
		d, _ := path[idx].Data()
		return d
	}
	leftData := combineRight(path, idx+1)
	if path[idx+1].IsLeft() {
		r, _ := path[idx].Right()
		d, _ := r.Data()
		return leftData.CombineWith(d)
	} else {
		return leftData
	}
}

func combineLeft(path []tree.Finger, idx int) tree.Data {
	if idx == len(path)-1 {
		d, _ := path[idx].Data()
		return d
	}
	rightData := combineLeft(path, idx+1)
	if path[idx+1].IsRight() {
		l, _ := path[idx].Left()
		d, _ := l.Data()
		return d.CombineWith(rightData)
	} else {
		return rightData
	}
}

// CombineBetween combines all nodes between the two pointed by f1 and f2 fingers. It returns error if any of the fingers is pointing to nil node.
func CombineBetween(f1, f2 tree.Finger) (tree.Data, error) {
	p1 := toSlice(f1)
	p2 := toSlice(f2)

	_, err1 := f1.Data()
	_, err2 := f2.Data()
	if err1 != nil || err2 != nil {
		return nil, errors.New("Cannot CombineBetween if one of the fingers is pointing to nil node")
	}

	i := 1
	for ; i < len(p1) && i < len(p2); i++ {
		if p1[i].IsLeft() != p2[i].IsLeft() {
			if p1[i].IsRight() {
				p1, p2 = p2, p1
			}
			break
		}
	}

	if i == len(p1) {
		return p1[i-1].Data()
	}

	if i == len(p2) {
		return p2[i-1].Data()
	}

	return combineRight(p1, i).CombineWith(combineLeft(p2, i)), nil
}

// DataCombiner is a function that combines data stored in two corresponding nodes.
type DataCombinator func(data1, data2 tree.Data) tree.Data

type combinedFinger struct {
	combinator       DataCombinator
	finger1, finger2 tree.Finger
}

type combinedTree struct {
	root combinedFinger
}

func (t combinedTree) Root() tree.Finger {
	return t.root
}

func (t combinedTree) Update(tree.Finger, tree.Data) (Tree, error) {
	return nil, errors.New("Combined trees cannot be updated")
}

func firstError(err1, err2 error) error {
	if err1 != nil {
		return err1
	}
	return err2
}

func (f combinedFinger) Data() (tree.Data, error) {
	data1, err1 := f.finger1.Data()
	data2, err2 := f.finger2.Data()
	err := firstError(err1, err2)
	if err != nil {
		return nil, err
	}
	return f.combinator(data1, data2), nil
}

func (f combinedFinger) IsLeaf() bool {
	return f.finger1.IsLeaf()
}

func (f combinedFinger) Left() (tree.Finger, error) {
	left1, err1 := f.finger1.Left()
	left2, err2 := f.finger2.Left()
	err := firstError(err1, err2)
	if err != nil {
		return nil, err
	}
	return combinedFinger{f.combinator, left1, left2}, nil
}

func (f combinedFinger) Right() (tree.Finger, error) {
	right1, err1 := f.finger1.Right()
	right2, err2 := f.finger2.Right()
	err := firstError(err1, err2)
	if err != nil {
		return nil, err
	}
	return combinedFinger{f.combinator, right1, right2}, nil
}

func (f combinedFinger) Up() (tree.Finger, error) {
	up1, err1 := f.finger1.Up()
	up2, err2 := f.finger2.Up()
	err := firstError(err1, err2)
	if err != nil {
		return nil, err
	}
	return combinedFinger{f.combinator, up1, up2}, nil
}

func (f combinedFinger) IsLeft() bool {
	return f.finger1.IsLeft()
}

func (f combinedFinger) IsRight() bool {
	return f.finger1.IsRight()
}

// CombineTrees takes two trees of the same shape and on-the-fly constructs a new tree in which each node has data produced by combining the data of corresponding nodes from tree1 and tree2. CombinedTrees cannot be updated.
func CombineTrees(combinator DataCombinator, tree1, tree2 tree.Tree) Tree {
	return combinedTree{combinedFinger{combinator, tree1.Root(), tree2.Root()}}
}
