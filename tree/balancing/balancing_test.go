package balancing

import (
	"bitbucket.org/sapalskimichal/persistent/tree"
	"math"
	"math/rand"
	"testing"
)

func TestEmpty(t *testing.T) {
	tr := Empty()
	r := tr.Root()
	if d, err := r.Data(); err == nil {
		t.Errorf("Root().Data() = (%v, %v), want (*, error)", d, err)
	}
	if l, err := r.Left(); err == nil {
		t.Errorf("Root().Left() = (%v, %v), want (*, error)", l, err)
	}
	if right, err := r.Right(); err == nil {
		t.Errorf("Root().Right() = (%v, %v), want (*, error)", right, err)
	}
	if up, err := r.Up(); err == nil {
		t.Errorf("Root().Up() = (%v, %v), want (*, error)", up, err)
	}
	if r.IsLeaf() {
		t.Errorf("Root().IsLeaf() = true, want false")
	}
}

type intData interface {
	Int() int
}

type sumInt int

func (a sumInt) CombineWith(other tree.Data) tree.Data {
	return a + other.(sumInt)
}
func (a sumInt) Int() int {
	return int(a)
}

type setInt int

func (aa setInt) CombineWith(other tree.Data) tree.Data {
	a := int(aa)
	digits := make(map[int]bool)
	for ; a != 0; a /= 10 {
		digits[a%10] = true
	}
	o := int(other.(setInt))
	for ; o != 0; o /= 10 {
		digits[o%10] = true
	}
	res := 0
	for i := 1; i <= 9; i++ {
		if digits[i] {
			res = 10*res + i
		}
	}
	return setInt(res)
}
func (a setInt) Int() int {
	return int(a)
}

func tour(tr tree.Tree, t *testing.T) []int {
	res := make([]int, 0)
	testUp := func(f tree.Finger, d tree.Data) {
		u, err := f.Up()
		if err != nil {
			t.Errorf("Up() = (%v, %v), want (..., nil)", u, err)
		}
		ud, err := u.Data()
		if err != nil || d != ud {
			t.Errorf("Data() = (%v, %v), want (%v, nil)", ud, err, d)
		}
	}
	testNil := func(f tree.Finger) {
		if d, err := f.Data(); err == nil {
			t.Errorf("Data() = (%v, %v), want (*, error)", d, err)
		}
	}
	var visit func(tree.Finger)
	visit = func(f tree.Finger) {
		d, err := f.Data()
		if err != nil {
			t.Errorf("Data() = (%v, %v), want (..., nil)", d, err)
			return
		}
		res = append(res, d.(intData).Int())
		left, err := f.Left()
		if err != nil {
			t.Errorf("Left() = (%v, %v), want (..., nil)", left, err)
			return
		}
		if !left.IsLeft() {
			t.Errorf("Left().IsLeft() = false, want true")
		}
		if left.IsRight() {
			t.Errorf("Left().IsRight() = true, want false")
		}
		testUp(left, d)

		right, err := f.Right()
		if err != nil {
			t.Errorf("Right() = (%v, %v), want (..., nil)", right, err)
			return
		}
		if !right.IsRight() {
			t.Errorf("Right().IsRight() = false, want true")
		}
		if right.IsLeft() {
			t.Errorf("Right().IsLeft() = true, want false")
		}
		testUp(right, d)

		if f.IsLeaf() {
			testNil(left)
			testNil(right)
		} else {
			visit(left)
			res = append(res, d.(intData).Int())
			visit(right)
			res = append(res, d.(intData).Int())
		}
	}
	r := tr.Root()
	if _, err := r.Data(); err != nil {
		return res
	}
	visit(tr.Root())
	return res
}

func same(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, va := range a {
		if va != b[i] {
			return false
		}
	}
	return true
}

func TestFromSlice(t *testing.T) {
	tr := FromSlice([]tree.Data{
		sumInt(10000),
		sumInt(2000),
		sumInt(300),
		sumInt(40),
		sumInt(5),
	})
	if d, err := tr.Root().Data(); err != nil || d.(sumInt) != 12345 {
		t.Errorf("Root().Data() = (%v, %v), want (12345, nil)", d, err)
	}
	expectedTour := []int{12345, 12000, 10000, 12000, 2000, 12000, 12345, 345, 340, 300, 340, 40, 340, 345, 5, 345, 12345}
	if actualTour := tour(tr, t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}
	checkInvariants(tr, t)

	tr = FromSlice([]tree.Data{})
	if d, err := tr.Root().Data(); err == nil {
		t.Errorf("Root().Data() = (%v, %v), want (*, error)", d, err)
	}

	tr = FromSlice([]tree.Data{sumInt(1)})
	expectedTour = []int{1}
	if actualTour := tour(tr, t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}
	if tr.Root().IsLeft() {
		t.Errorf("Root().IsLeft() = true, want false")
	}
	if tr.Root().IsRight() {
		t.Errorf("Root().IsRight() = true, want false")
	}

	tr = FromSlice([]tree.Data{
		setInt(1),
		setInt(2),
		setInt(3),
		setInt(4),
		setInt(5),
		setInt(6),
		setInt(7),
	})
	checkInvariants(tr, t)
}

func TestFromSliceAll(t *testing.T) {
	d := []tree.Data{}
	for i := 0; i < 200; i++ {
		tr := FromSlice(d)
		checkInvariants(tr, t)
		d = append(d, setInt(i))
	}
}

func TestMerge(t *testing.T) {
	tree1 := FromSlice([]tree.Data{
		sumInt(20),
		sumInt(1),
	})
	tree2 := Empty()

	expectedTour := []int{21, 20, 21, 1, 21}
	if actualTour := tour(tree1, t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}
	if actualTour := tour(tree1.Merge(tree2), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}
	if actualTour := tour(tree2.Merge(tree1), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	tree1 = FromSlice([]tree.Data{
		setInt(2),
		setInt(1),
	})
	tree2 = FromSlice([]tree.Data{
		setInt(3),
		setInt(4),
		setInt(5),
	})
	expectedTour = []int{12345, 12, 2, 12, 1, 12, 12345, 345, 34, 3, 34, 4, 34, 345, 5, 345, 12345}
	if actualTour := tour(tree1.Merge(tree2), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}
	expectedTour = []int{12345, 345, 34, 3, 34, 4, 34, 345, 5, 345, 12345, 12, 2, 12, 1, 12, 12345}
	if actualTour := tour(tree2.Merge(tree1), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	tree1 = FromSlice([]tree.Data{
		setInt(1),
		setInt(2),
		setInt(3),
		setInt(4),
	})
	tree2 = FromSlice([]tree.Data{
		setInt(5),
	})
	expectedTour = []int{12345, 12, 1, 12, 2, 12, 12345, 345, 34, 3, 34, 4, 34, 345, 5, 345, 12345}
	if actualTour := tour(tree1.Merge(tree2), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	tree1 = tree1.Merge(tree2)
	tree2 = FromSlice([]tree.Data{
		setInt(6),
	})
	expectedTour = []int{123456, 1234, 12, 1, 12, 2, 12, 1234, 34, 3, 34, 4, 34, 1234, 123456, 56, 5, 56, 6, 56, 123456}
	if actualTour := tour(tree1.Merge(tree2), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	tree1 = tree1.Merge(tree2)
	tree2 = FromSlice([]tree.Data{
		setInt(7),
		setInt(8),
	})
	expectedTour = []int{12345678, 1234, 12, 1, 12, 2, 12, 1234, 34, 3, 34, 4, 34, 1234, 12345678, 5678, 56, 5, 56, 6, 56, 5678, 78, 7, 78, 8, 78, 5678, 12345678}
	if actualTour := tour(tree1.Merge(tree2), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	tree1 = tree1.Merge(tree2)
	tree2 = FromSlice([]tree.Data{
		setInt(9),
	})
	oldExpectedTour := expectedTour
	expectedTour = []int{123456789, 1234, 12, 1, 12, 2, 12, 1234, 34, 3, 34, 4, 34, 1234, 123456789, 56789, 56, 5, 56, 6, 56, 56789, 789, 78, 7, 78, 8, 78, 789, 9, 789, 56789, 123456789}
	if actualTour := tour(tree1.Merge(tree2), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}
	if actualTour := tour(tree1, t); !same(actualTour, oldExpectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, oldExpectedTour)
	}

	tree2 = FromSlice([]tree.Data{
		setInt(3),
		setInt(4),
		setInt(5),
		setInt(6),
	})
	tree1 = FromSlice([]tree.Data{
		setInt(2),
	})
	expectedTour = []int{23456, 234, 23, 2, 23, 3, 23, 234, 4, 234, 23456, 56, 5, 56, 6, 56, 23456}
	if actualTour := tour(tree1.Merge(tree2), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	tree2 = tree1.Merge(tree2)
	tree1 = FromSlice([]tree.Data{
		setInt(1),
	})
	expectedTour = []int{123456, 1234, 12, 1, 12, 2, 12, 1234, 34, 3, 34, 4, 34, 1234, 123456, 56, 5, 56, 6, 56, 123456}
	if actualTour := tour(tree1.Merge(tree2), t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}
}

func checkInvariants(trr Tree, t *testing.T) {
	tr := trr.(balancingTree)
	var check func(n, p *node) int
	check = func(n, p *node) int {
		if n == nil {
			return 0
		}
		if n.red && (p == nil || p.left != n || p.red) {
			t.Errorf("A red node should be the left child of a black node.")
		}
		if n.red && (n.left == nil || n.right == nil) {
			t.Errorf("All leaves should be black.")
		}
		lh := check(n.left, n)
		rh := check(n.right, n)
		if lh != rh {
			t.Errorf("Number of black nodes on any path from leaf to root should be the same (%v vs %v).", lh, rh)
		}
		if n.red {
			return lh
		}
		return lh + 1
	}
	check(tr.root, nil)
}

type stringCat string

func (ss stringCat) CombineWith(other tree.Data) tree.Data {
	s := string(ss)
	o := string(other.(stringCat))
	return stringCat(s + o)
}

func randomString() string {
	r := ""
	for i := 0; i < 4; i++ {
		r += string(rune('a' + rand.Intn(8)))
	}
	return r
}

func stringToTree(s string) Tree {
	letters := make([]tree.Data, len(s))
	for i, l := range s {
		letters[i] = stringCat(string(l))
	}
	return FromSlice(letters)
}

func TestMergeRandom(t *testing.T) {
	n, m := 80, 20
	for i := 0; i < n; i++ {
		res := ""
		trees := make([]Tree, m)
		for j := 0; j < m; j++ {
			s := randomString()
			res += s
			trees[j] = stringToTree(s)
		}
		for tr := len(trees); tr > 1; tr = len(trees) {
			a := rand.Intn(tr - 1)
			trees[a] = trees[a].Merge(trees[a+1])
			checkInvariants(trees[a], t)
			for k := a + 1; k < tr-1; k++ {
				trees[k] = trees[k+1]
			}
			trees = trees[:tr-1]
		}
		if d, err := trees[0].Root().Data(); err != nil || string(d.(stringCat)) != res {
			t.Errorf("Merge() = (%v, %v), want (%v, nil)", d, err, res)
		}
	}
}

func randomSumIntTree(n int, values []int) (Tree, []int) {
	if n < 5 {
		data := make([]tree.Data, n)
		for i := 0; i < n; i++ {
			v := rand.Intn(10000)
			values = append(values, v)
			data[i] = sumInt(v)
		}
		return FromSlice(data), values
	}
	left := 1 + rand.Intn(n-1)
	lt, values := randomSumIntTree(left, values)
	rt, values := randomSumIntTree(n-left, values)
	return lt.Merge(rt), values
}

func BenchmarkMerge(b *testing.B) {
	n, m, f := 40, 5.0, 1.7
	trees := make([]Tree, n)
	for i := 0; i < n; i++ {
		trees[i], _ = randomSumIntTree(int(m*math.Pow(float64(f), float64(i/2))), make([]int, 0))
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		trees[a].Merge(trees[b])
	}
}

func TestSplit(t *testing.T) {
	tr := FromSlice([]tree.Data{
		setInt(1),
		setInt(2),
		setInt(3),
		setInt(4),
		setInt(5),
		setInt(6),
		setInt(7),
		setInt(8),
		setInt(9),
	})

	expectedTour := []int{123456789, 1234, 12, 1, 12, 2, 12, 1234, 34, 3, 34, 4, 34, 1234, 123456789, 56789, 56, 5, 56, 6, 56, 56789, 789, 78, 7, 78, 8, 78, 789, 9, 789, 56789, 123456789}
	if actualTour := tour(tr, t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	f := tr.Root()
	f, _ = f.Left()
	f, _ = f.Right()
	f, _ = f.Right()

	left, right, err := tr.SplitLeft(f)
	if err != nil {
		t.Errorf("SplitLeft() = (%v, %v, %v), want (..., ..., nil)", left, right, err)
	}

	leftTour := []int{1234, 12, 1, 12, 2, 12, 1234, 34, 3, 34, 4, 34, 1234}
	if actualTour := tour(left, t); !same(actualTour, leftTour) {
		t.Errorf("left.tour() = %v, want %v", actualTour, leftTour)
	}
	rightTour := []int{56789, 56, 5, 56, 6, 56, 56789, 789, 78, 7, 78, 8, 78, 789, 9, 789, 56789}
	if actualTour := tour(right, t); !same(actualTour, rightTour) {
		t.Errorf("right.tour() = %v, want %v", actualTour, rightTour)
	}

	left, right, err = tr.SplitRight(f)
	if err != nil {
		t.Errorf("SplitRight() = (%v, %v, %v), want (..., ..., nil)", left, right, err)
	}

	leftTour = []int{123, 12, 1, 12, 2, 12, 123, 3, 123}
	if actualTour := tour(left, t); !same(actualTour, leftTour) {
		t.Errorf("left.tour() = %v, want %v", actualTour, leftTour)
	}
	rightTour = []int{456789, 456, 45, 4, 45, 5, 45, 456, 6, 456, 456789, 789, 78, 7, 78, 8, 78, 789, 9, 789, 456789}
	if actualTour := tour(right, t); !same(actualTour, rightTour) {
		t.Errorf("right.tour() = %v, want %v", actualTour, rightTour)
	}

	f, _ = f.Left()
	left, right, err = tr.SplitLeft(f)
	if err == nil {
		t.Errorf("SplitLeft() = (%v, %v, %v), want (*, *, error)", left, right, err)
	}
	left, right, err = tr.SplitRight(f)
	if err == nil {
		t.Errorf("SplitRight() = (%v, %v, %v), want (*, *, error)", left, right, err)
	}
}

func leftmost(f tree.Finger) tree.Finger {
	for !f.IsLeaf() {
		f, _ = f.Left()
	}
	return f
}

func next(f tree.Finger) tree.Finger {
	for f.IsRight() {
		f, _ = f.Up()
	}
	f, _ = f.Up()
	f, _ = f.Right()
	return leftmost(f)
}

func kth(t Tree, k int) tree.Finger {
	f := leftmost(t.Root())
	for i := 0; i < k; i++ {
		f = next(f)
	}
	return f
}

func sumValue(t Tree) int {
	d, err := t.Root().Data()
	if err != nil {
		return 0
	}
	return int(d.(sumInt))
}

func TestSplitRandom(t *testing.T) {
	n, m, s := 40, 40, 40
	for i := 0; i < n; i++ {
		tr, values := randomSumIntTree(s, make([]int, 0))
		checkInvariants(tr, t)
		for j := 0; j < m; j++ {
			k := rand.Intn(s)
			f := kth(tr, k)
			leftSum, rightSum := 0, 0
			d, _ := f.Data()
			mid := int(d.(sumInt))

			for l, v := range values {
				if l < k {
					leftSum += v
				}
				if l > k {
					rightSum += v
				}
			}

			if left, right, err := tr.SplitLeft(f); err != nil || sumValue(left) != leftSum+mid || sumValue(right) != rightSum {
				t.Errorf("SplitLeft(%v) = (%v, %v, %v), want (%v, %v, nil)", k, sumValue(left), sumValue(right), err, leftSum+mid, rightSum)
			}

			if left, right, err := tr.SplitRight(f); err != nil || sumValue(left) != leftSum || sumValue(right) != rightSum+mid {
				t.Errorf("SplitRight(%v) = (%v, %v, %v), want (%v, %v, nil)", k, sumValue(left), sumValue(right), err, leftSum, rightSum+mid)
			}
		}
	}
}

func BenchmarkSplit(b *testing.B) {
	n := 100000
	tr, _ := randomSumIntTree(n, make([]int, 0))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		f := randomLeaf(tr)
		tr.SplitLeft(f)
	}
}

func TestUpdate(t *testing.T) {
	tr := FromSlice([]tree.Data{
		setInt(1),
		setInt(2),
		setInt(3),
		setInt(4),
	})

	expectedTour := []int{1234, 12, 1, 12, 2, 12, 1234, 34, 3, 34, 4, 34, 1234}
	if actualTour := tour(tr, t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	f := tr.Root()
	f, _ = f.Left()
	f, _ = f.Right()

	t2, err := tr.Update(f, setInt(7))
	if err != nil {
		t.Errorf("Update(f, 7) = (%v, %v), want (..., nil)", t2, err)
	}
	if actualTour := tour(tr, t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	expectedTour = []int{1347, 17, 1, 17, 7, 17, 1347, 34, 3, 34, 4, 34, 1347}
	if actualTour := tour(t2, t); !same(actualTour, expectedTour) {
		t.Errorf("tour() = %v, want %v", actualTour, expectedTour)
	}

	f, _ = f.Left()
	t2, err = t2.Update(f, setInt(7))
	if err == nil {
		t.Errorf("Update(f, 7) = (%v, %v), want (*, error)", t2, err)
	}
}

func TestUpdateRandom(t *testing.T) {
	n, m, s := 40, 40, 40
	for i := 0; i < n; i++ {
		tr, _ := randomSumIntTree(s, make([]int, 0))
		baseValue := sumValue(tr)
		checkInvariants(tr, t)
		for j := 0; j < m; j++ {
			k := rand.Intn(s)
			f := kth(tr, k)
			d, _ := f.Data()
			value := int(d.(sumInt))

			t2, err := tr.Update(f, sumInt(10))
			checkInvariants(t2, t)
			if sumValue(tr) != baseValue {
				t.Errorf("Updte changed original tree!")
			}
			if err != nil || sumValue(t2) != baseValue-value+10 {
				t.Errorf("Update(%v) = (%v, %v), want (%v, nil)", k, sumValue(t2), baseValue-value+10)
			}
		}
	}
}

func BenchmarkUpdate(b *testing.B) {
	n := 100000
	tr, _ := randomSumIntTree(n, make([]int, 0))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		f := randomLeaf(tr)
		tr.Update(f, sumInt(100))
	}
}

func TestInsert(t *testing.T) {
	t1 := Empty()
	expectedTour := []int{}
	f := t1.Root()

	t2, err := t1.Insert(f, setInt(1))
	if actualTour := tour(t1, t); !same(actualTour, expectedTour) {
		t.Errorf("Insert changed the original tree! Is: %v, want %v", actualTour, expectedTour)
	}
	expectedTour = []int{1}
	if actualTour := tour(t2, t); err != nil || !same(actualTour, expectedTour) {
		t.Errorf("Insert(1) = (%v, %v) want (%v, nil)", actualTour, err, expectedTour)
	}

	f = t2.Root()
	f, _ = f.Right()
	t3, err := t2.Insert(f, setInt(2))
	expectedTour = []int{12, 1, 12, 2, 12}
	if actualTour := tour(t3, t); err != nil || !same(actualTour, expectedTour) {
		t.Errorf("Insert(2) = (%v, %v) want (%v, nil)", actualTour, err, expectedTour)
	}

	f = t3.Root()
	f, _ = f.Left()
	f, _ = f.Left()
	t4, err := t3.Insert(f, setInt(3))
	expectedTour = []int{123, 13, 3, 13, 1, 13, 123, 2, 123}
	if actualTour := tour(t4, t); err != nil || !same(actualTour, expectedTour) {
		t.Errorf("Insert(3) = (%v, %v) want (%v, nil)", actualTour, err, expectedTour)
	}

	f, _ = f.Up()
	if t5, err := t3.Insert(f, setInt(3)); err == nil {
		t.Errorf("Insert(3) = (%v, %v) want (*, error))", t5, err)
	}
}

func TestInsertRandom(t *testing.T) {
	n, m := 40, 40
	for i := 0; i < n; i++ {
		t1 := Empty()
		sum := 0
		for j := 0; j < m; j++ {
			f := randomNilNode(t1)
			v := rand.Intn(1000)

			t2, err := t1.Insert(f, sumInt(v))
			if sumValue(t1) != sum {
				t.Errorf("Insert changed the original tree! Is: %v, want %v", sumValue(t1), sum)
			}

			t1 = t2
			sum += v
			if err != nil || sumValue(t1) != sum {
				t.Errorf("Insert(%v) = (%v, %v), want (%v, nil)", v, sumValue(t1), err, sum)
			}
			checkInvariants(t1, t)
		}
	}
}

func BenchmarkInsert(b *testing.B) {
	n := 100000
	base, _ := randomSumIntTree(n, make([]int, 0))
	b.ResetTimer()
	tr := base
	for i := 0; i < b.N; i++ {
		if i%n == 0 {
			tr = base
		}
		f := randomNilNode(tr)
		tr, _ = tr.Insert(f, sumInt(100))
	}
}

func TestDelete(t *testing.T) {
	t1 := FromSlice([]tree.Data{
		setInt(1),
		setInt(2),
		setInt(3),
	})
	f := t1.Root()
	expectedTour := []int{123, 12, 1, 12, 2, 12, 123, 3, 123}

	t2, err := t1.Delete(f)
	if actualTour := tour(t1, t); !same(actualTour, expectedTour) {
		t.Errorf("Delete() changed the original tree! Is: %v, want %v", actualTour, expectedTour)
	}
	if err == nil {
		t.Errorf("Delete() = (%v, %v), want (*, error)", t2, err)
	}

	f, _ = t1.Root().Left()
	f, _ = f.Right()
	expectedTour = []int{13, 1, 13, 3, 13}
	t2, err = t1.Delete(f)
	if actualTour := tour(t2, t); err != nil || !same(actualTour, expectedTour) {
		t.Errorf("Delete(2) = (%v, %v), want (%v, nil)", actualTour, err, expectedTour)
	}

	f, _ = t2.Root().Right()
	expectedTour = []int{1}
	t3, err := t2.Delete(f)
	if actualTour := tour(t3, t); err != nil || !same(actualTour, expectedTour) {
		t.Errorf("Delete(3) = (%v, %v), want (%v, nil)", actualTour, err, expectedTour)
	}

	f = t3.Root()
	expectedTour = []int{}
	t4, err := t3.Delete(f)
	if actualTour := tour(t4, t); err != nil || !same(actualTour, expectedTour) {
		t.Errorf("Delete(1) = (%v, %v), want (%v, nil)", actualTour, err, expectedTour)
	}
}

func randomLeaf(t Tree) tree.Finger {
	f := t.Root()
	for !f.IsLeaf() {
		if 0 == rand.Intn(2) {
			f, _ = f.Left()
		} else {
			f, _ = f.Right()
		}
	}
	return f
}

func randomNilNode(t Tree) tree.Finger {
	f := t.Root()
	for f.(finger).pointed() != nil {
		if 0 == rand.Intn(2) {
			f, _ = f.Left()
		} else {
			f, _ = f.Right()
		}
	}
	return f
}

func TestDeleteRandom(t *testing.T) {
	n, m := 40, 40
	for i := 0; i < n; i++ {
		t1, values := randomSumIntTree(m, make([]int, 0))
		sum := 0
		for _, v := range values {
			sum += v
		}
		for j := 0; j < m; j++ {
			f := randomLeaf(t1)
			d, _ := f.Data()
			v := int(d.(sumInt))

			t2, err := t1.Delete(f)
			if sumValue(t1) != sum {
				t.Errorf("Delete changed the original tree! Is: %v, want %v", sumValue(t1), sum)
			}

			t1 = t2
			sum -= v
			if err != nil || sumValue(t1) != sum {
				t.Errorf("Delete(%v) = (%v, %v), want (%v, nil)", v, sumValue(t1), err, sum)
			}
			checkInvariants(t1, t)
		}
	}
}

func BenchmarkDelete(b *testing.B) {
	n := 200000
	base, _ := randomSumIntTree(n, make([]int, 0))
	b.ResetTimer()
	tr := base
	for i := 0; i < b.N; i++ {
		if i%(n/2) == 0 {
			tr = base
		}
		f := randomLeaf(tr)
		tr, _ = tr.Delete(f)
	}
}
