// balancing package implements self-balancing binary tree. The implementation is a translation of 2-3 tree into binary trees. For more details see TODO: url.
package balancing

import (
	"bitbucket.org/sapalskimichal/persistent/list"
	"bitbucket.org/sapalskimichal/persistent/tree"
	"errors"
)

// Tree is an interface for a~balancing tree.
type Tree interface {
	tree.Tree
	// Merge merges this tree with other tree. It returns a new balancing tree which stores first all of the leaves from this tree and then all of the leaves from the other tree. Note that merging trees with different data types may result in runtime panic.
	Merge(other Tree) Tree
	// SplitLeft splits the tree at the given finger. The finger has to point to a leaf or an error will be returned. All leaves to the left of the finger (including the finger) will be put into left tree, the other leaves will be put into right tree.
	SplitLeft(f tree.Finger) (left Tree, right Tree, err error)
	// SplitRight splits the tree at the given finger. The finger has to point to a leaf or an error will be returned. All leaves to the left of the finger (NOT including the finger) will be put into left tree, the other leaves will be put into right tree.
	SplitRight(f tree.Finger) (left Tree, right Tree, err error)
	// Update creates a new Tree with the leaf pointed by the finger updated to contain given data. If finger does not point to a leaf an error is returned.
	Update(f tree.Finger, newData tree.Data) (Tree, error)
	// Insert creates a new Tree with a new leaf at the place pointed by the finger. The new leaf will contain specified data.
	// If finger does not point to a nil node an error is returned.
	Insert(f tree.Finger, data tree.Data) (Tree, error)
	// Delete creates a new Tree with the leaf pointed by the finger removed. If the finger does not point to a leaf an error is returned.
	Delete(f tree.Finger) (Tree, error)
}

type balancingTree struct {
	root   *node
	height int
}

type node struct {
	data        tree.Data
	red         bool
	left, right *node
}

type finger struct {
	path     list.List
	leftNil  bool
	rightNil bool
}

func (f finger) pointed() *node {
	return f.path.Head().(*node)
}

func (f finger) parent() *node {
	if f.path.Len() < 2 {
		return nil
	}
	return f.path.Tail().Head().(*node)
}

var noSuchNode error = errors.New("No such node")

func (f finger) Data() (tree.Data, error) {
	p := f.pointed()
	if p == nil {
		return nil, noSuchNode
	}
	return p.data, nil
}

func (f finger) Left() (tree.Finger, error) {
	p := f.pointed()
	if p == nil {
		return nil, noSuchNode
	}
	return finger{f.path.Push(p.left), p.left == nil, false}, nil
}

func (f finger) Right() (tree.Finger, error) {
	p := f.pointed()
	if p == nil {
		return nil, noSuchNode
	}
	return finger{f.path.Push(p.right), false, p.right == nil}, nil
}

func (f finger) Up() (tree.Finger, error) {
	p := f.parent()
	if p == nil {
		return nil, noSuchNode
	}
	return finger{f.path.Tail(), false, false}, nil
}

func (f finger) IsLeaf() bool {
	p := f.pointed()
	return p != nil && p.left == nil && p.right == nil
}

func (f finger) IsLeft() bool {
	t := f.pointed()
	if t == nil {
		return f.leftNil
	}
	p := f.parent()
	if p != nil {
		return p.left == t
	}
	return false
}

func (f finger) IsRight() bool {
	t := f.pointed()
	if t == nil {
		return f.rightNil
	}
	p := f.parent()
	if p != nil {
		return p.right == t
	}
	return false
}

func (t balancingTree) Root() tree.Finger {
	return finger{list.New().Push(t.root), false, false}
}

func getSubtrees(t balancingTree, reverse bool) []balancingTree {
	res := make([]balancingTree, 0, 3)
	h := t.height - 1
	if t.root.left.red {
		res = append(res, balancingTree{t.root.left.left, h})
		res = append(res, balancingTree{t.root.left.right, h})
	} else {
		res = append(res, balancingTree{t.root.left, h})
	}
	res = append(res, balancingTree{t.root.right, h})
	if reverse {
		res[0], res[len(res)-1] = res[len(res)-1], res[0]
	}
	return res
}

func mergeHelper(left, right balancingTree, reverse bool) (balancingTree, balancingTree) {
	if left.height == right.height {
		return left, right
	}
	trees := getSubtrees(left, reverse)
	n := len(trees)
	if left.height == right.height+1 {
		if n == 2 {
			return combineTrees(reverse, trees[0], trees[1], right), balancingTree{}
		} else {
			return combineTrees(reverse, trees[0], trees[1]), combineTrees(reverse, trees[2], right)
		}
	} else {
		a, b := mergeHelper(trees[n-1], right, reverse)
		trees[n-1] = a
		if b.height == 0 {
			return combineTrees(reverse, trees...), b
		} else {
			return mergeHelper(combineTrees(reverse, trees...), b, reverse)
		}
	}
}

func combineTrees(reverse bool, trees ...balancingTree) balancingTree {
	n := len(trees)
	if reverse {
		trees[0], trees[n-1] = trees[n-1], trees[0]
	}
	if n == 3 {
		left := combineTrees(false, trees[0], trees[1])
		left.root.red = true
		return balancingTree{combineNodes(left.root, trees[2].root), left.height}
	}
	return balancingTree{combineNodes(trees[0].root, trees[1].root), trees[0].height + 1}
}

func (t balancingTree) Merge(other Tree) Tree {
	o := other.(balancingTree)
	reverse := false
	if o.height > t.height {
		reverse = true
		o, t = t, o
	}
	if o.height == 0 {
		return t
	}
	left, right := mergeHelper(t, o, reverse)
	if right.height == 0 {
		return left
	}
	return combineTrees(reverse, left, right)
}

func splitPath(f tree.Finger) ([]Tree, Tree, []Tree) {
	d, _ := f.Data()
	mid := FromSlice([]tree.Data{d})
	lTrees, rTrees := make([]Tree, 0, 12), make([]Tree, 0, 12)
	h := 1
	for _, err := f.Up(); err == nil; _, err = f.Up() {
		parent := f.(finger).parent()
		if f.IsLeft() {
			rTrees = append(rTrees, balancingTree{parent.right, h})
		} else {
			if parent.left.red {
				lTrees = append(lTrees, balancingTree{parent.left.right, h})
				lTrees = append(lTrees, balancingTree{parent.left.left, h})
			} else {
				lTrees = append(lTrees, balancingTree{parent.left, h})
			}
		}
		fp := f
		if parent.red {
			fp, _ = f.Up()
			gp := fp.(finger).parent()
			rTrees = append(rTrees, balancingTree{gp.right, h})
		}
		f, _ = fp.Up()
		h++
	}

	return lTrees, mid, rTrees
}

func split(f tree.Finger, putLeft bool, putRight bool) (Tree, Tree, error) {
	if !f.IsLeaf() {
		return nil, nil, errors.New("Cannot split in a non-leaf node.")
	}
	lTrees, mTree, rTrees := splitPath(f)
	left, right := Empty(), Empty()
	if putLeft {
		left = mTree
	}
	if putRight {
		right = mTree
	}
	for _, t := range lTrees {
		left = t.Merge(left)
	}
	for _, t := range rTrees {
		right = right.Merge(t)
	}
	return left, right, nil
}

func (balancingTree) SplitLeft(f tree.Finger) (Tree, Tree, error) {
	return split(f, true, false)
}

func (balancingTree) SplitRight(f tree.Finger) (Tree, Tree, error) {
	return split(f, false, true)
}

func (t balancingTree) Update(f tree.Finger, newData tree.Data) (Tree, error) {
	if !f.IsLeaf() {
		return nil, errors.New("Cannot update a non-leaf node.")
	}
	ff := f.(finger)
	v := &node{newData, false, nil, nil}
	for p := ff.parent(); p != nil; {
		left, right := p.left, p.right
		if ff.IsLeft() {
			left = v
		} else {
			right = v
		}
		v = combineNodes(left, right)
		v.red = p.red

		f, _ := ff.Up()
		ff = f.(finger)
		p = ff.parent()
	}
	return balancingTree{v, t.height}, nil
}

func (balancingTree) Insert(f tree.Finger, data tree.Data) (Tree, error) {
	if f.(finger).pointed() != nil {
		return nil, errors.New("Cannot insert new leaf: finger pointing to a non-nil node.")
	}
	singleton := FromSlice([]tree.Data{data})
	p, err := f.Up()
	if err != nil {
		return singleton, nil
	}

	left, right, _ := split(p, f.IsRight(), f.IsLeft())
	return left.Merge(singleton).Merge(right), nil
}

func (balancingTree) Delete(f tree.Finger) (Tree, error) {
	left, right, err := split(f, false, false)
	if err != nil {
		return nil, err
	}
	return left.Merge(right), nil
}

// Empty returns an empty self-balancing binary tree.
func Empty() Tree {
	return FromSlice([]tree.Data{})
}

func combineNodes(a, b *node) *node {
	return &node{a.data.CombineWith(b.data), false, a, b}
}

func log(n int) int {
	res := 0
	for n != 0 {
		res++
		n /= 2
	}
	return res
}

func nodesFromSlice(data []tree.Data, red bool) (*node, int) {
	if len(data) == 0 {
		return nil, 0
	}
	if len(data) == 1 {
		return &node{data[0], false, nil, nil}, 1
	}
	if len(data) == 2 {
		return combineNodes(&node{data[0], false, nil, nil}, &node{data[1], false, nil, nil}), 2
	}

	n := len(data)
	leftSize := n / 2
	var left *node
	if log(leftSize) != log(n-leftSize) && !red {
		leftSize = n - n/3
		left, _ = nodesFromSlice(data[:leftSize], true)
		left.red = true
	} else {
		left, _ = nodesFromSlice(data[:leftSize], false)
	}
	right, height := nodesFromSlice(data[leftSize:], false)
	return combineNodes(left, right), height + 1
}

// FromSlice returns a self-balancing binary tree with given data in its leaves.
func FromSlice(data []tree.Data) Tree {
	root, height := nodesFromSlice(data, false)
	return balancingTree{root, height}
}
