package kth_element

import (
	"bitbucket.org/sapalskimichal/persistent/examples/sum"
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"math"
	"math/rand"
	"sort"
	"strconv"
	"testing"
)

func randomElement(n int) string {
	return strconv.Itoa(rand.Intn(n))
}

func randomRangeKthElement(n int) (RangeKthElement, []string) {
	values := make([]string, n, n)
	for i := 0; i < n; i++ {
		values[i] = randomElement(n)
	}
	return NewRangeKthElement(values), values
}

func randomRange(n int) (a, b int) {
	a, b = rand.Intn(n), rand.Intn(n)
	if a > b {
		a, b = b, a
	}
	return
}

func TestRangeKthElementQuery(t *testing.T) {
	v := []string{"b", "k", "d", "s", "h", "f", "c", "n", "w"}
	r := NewRangeKthElement(v)

	if result := r.KthElement(3, 2, 6); result != "h" {
		t.Errorf("(%#v).KthElement(3, 2, 6) = %#v, want \"h\"", v, result)
	}

	if result := r.KthElement(1, 0, 3); result != "d" {
		t.Errorf("(%#v).KthElement(1, 0, 3) = %#v, want \"d\"", v, result)
	}
}

func bruteForceRangeKthElement(values []string, k, a, b int) string {
	ord := make([]string, b-a+1)
	for i := a; i <= b; i++ {
		ord[i-a] = values[i]
	}
	sort.Strings(ord)
	return ord[k]
}

func TestRangeKthElementBruteForce(t *testing.T) {
	n, m := 30, 40
	for i := 1; i <= n; i++ {
		ds, v := randomRangeKthElement(i)
		for j := 0; j < m; j++ {
			a, b := randomRange(i)
			k := rand.Intn(b - a + 1)
			ans := bruteForceRangeKthElement(v, k, a, b)
			if res := ds.KthElement(k, a, b); res != ans {
				t.Errorf("(%v).KthElement(%v, %v, %v) = %#v, want %#v", v, k, a, b, res, ans)
			}
		}
	}
}

func BenchmarkRangeKthElement(b *testing.B) {
	n := 100000
	ds, _ := randomRangeKthElement(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		k := rand.Intn(b - a)
		ds.KthElement(k, a, b)
	}
}

func randomPathKthElement(n int) (PathKthElement, sum.PathSum, tree.Tree) {
	depth := int(math.Sqrt(float64(n)) + 2)
	t := tree.RandomTree(n, depth)
	s := t.Copy()
	visitT := func(node *tree.Node) {
		p := node.ParentEdge
		if p != nil {
			p.Data = randomElement(n)
		}
	}
	t.DFS(visitT, tree.EmptyVisitor, tree.EmptyVisitor)
	visitS := func(node *tree.Node) {
		p := node.ParentEdge
		if p != nil {
			p.Data = 1
		}
	}
	s.DFS(visitS, tree.EmptyVisitor, tree.EmptyVisitor)
	return NewPathKthElement(t), sum.NewPathSum(s), t
}

func bruteForcePathKthElement(t tree.Tree, k, a, b int) string {
	ord := []string{}
	visitor := func(v, p *tree.Node) {
		if v.Id() == a || v.Id() == b {
			if v.Id() == a && v.Id() == b {
				a = -1
				b = -1
			} else {
				s := v.ParentEdge.Data.(string)
				ord = append(ord, s)
			}
		}
		if v.Id() == a {
			a = p.Id()
		}
		if v.Id() == b {
			b = p.Id()
		}
	}
	t.ParentDFS(tree.EmptyParentVisitor, tree.EmptyParentVisitor, visitor)
	sort.Strings(ord)
	return ord[k]
}

func TestPathKthElementBruteForce(t *testing.T) {
	n, m := 20, 60
	for i := 1; i <= n; i++ {
		ds, sum, tr := randomPathKthElement(i)
		for j := 0; j < m; j++ {
			a, b := rand.Intn(i), rand.Intn(i)
			length := sum.Sum(a, b)
			if length == 0 {
				continue
			}
			k := rand.Intn(length)
			ans := bruteForcePathKthElement(tr, k, a, b)
			if res := ds.KthElement(k, a, b); res != ans {
				t.Errorf("Test %v.%v: KthElement(%v, %v, %v) = %#v, want %#v", i, j, k, a, b, res, ans)
			}
		}
	}
}

func BenchmarkPathKthElement(b *testing.B) {
	n := 100000
	ds, sum, _ := randomPathKthElement(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		length := sum.Sum(a, b)
		if length == 0 {
			i--
			continue
		}
		k := rand.Intn(length)
		ds.KthElement(k, a, b)
	}
}
