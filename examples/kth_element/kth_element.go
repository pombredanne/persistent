// Package kth_element implements solution for range k-th element query problem and path k-th element query problem.
package kth_element

import (
	"bitbucket.org/sapalskimichal/persistent/examples/framework"
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"bitbucket.org/sapalskimichal/persistent/treemap"
)

// RangeKthElement represents solution to range k-th element problem: given an array, preprocess it to quickly answer KthElement queries.
type RangeKthElement interface {
	// RangeKthElement returns k-th smallest element from the range [a,b] in the underlying array. In other words: if the range [a,b] of the array was sorted, the element returned by RangeKthElement would be on position k.
	KthElement(k, a, b int) string
}

type rangeKthElement struct {
	rangeQuery framework.RangeQuery
}

func (r *rangeKthElement) KthElement(k, a, b int) string {
	return r.rangeQuery.Query(a, b, k).(string)
}

// NewRangeKthElement construct a solution to range k-th element problem for a given array.
func NewRangeKthElement(items []string) RangeKthElement {
	elements := make([]framework.Element, 0, len(items))
	itemsMap := make(map[string]int)
	for _, v := range items {
		elements = append(elements, v)
		itemsMap[v] = 0
	}

	initSweep := func([]framework.Element) framework.Sweep {
		return treemap.NewStaticStringSummingMap(itemsMap)
	}
	updateSweep := func(e framework.Element, s framework.Sweep) framework.Sweep {
		m := s.(treemap.Map)
		count, _ := m.Get(e)
		sweep, _ := s.(treemap.SummingMap).Update(e, count.(int)+1)
		return sweep
	}
	answerQuery := func(aSweep, bSweep framework.Sweep, args ...framework.Argument) framework.QueryResult {
		k := args[0].(int)
		t := treemap.Subtract(bSweep.(treemap.SummingMap), aSweep.(treemap.SummingMap))
		element, _ := t.FindByPrefixSum(k + 1)
		return element
	}
	return &rangeKthElement{framework.NewRangeQuery(elements, initSweep, updateSweep, answerQuery)}
}

// PathKthElement implements solution to path k-th element problem: preprocess a tree to quickly answer k-th element queries.
type PathKthElement interface {
	// KthElement returns k-th smallest element on path between nodes a and b. See RangeKthElement.KthElement.
	KthElement(k, a, b int) string
}

type pathKthElement struct {
	pathQuery framework.PathQuery
}

func (p *pathKthElement) KthElement(k, a, b int) string {
	return p.pathQuery.Query(a, b, k).(string)
}

// NewPathKthElement returns a solution for path k-th element problem for a given tree.
func NewPathKthElement(t tree.Tree) PathKthElement {
	elementMap := make(map[string]int)
	visit := func(n *tree.Node) {
		p := n.ParentEdge
		if p != nil {
			elementMap[p.Data.(string)] = 0
		}
	}
	t.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)

	initSweep := func(_ tree.Tree) framework.Sweep {
		return treemap.NewStaticStringSummingMap(elementMap)
	}
	updateSweep := func(n *tree.Node, s framework.Sweep) framework.Sweep {
		e := n.ParentEdge.Data.(string)
		m := s.(treemap.Map)
		count, _ := m.Get(e)
		sweep, _ := s.(treemap.SummingMap).Update(e, count.(int)+1)
		return sweep
	}
	answerQuery := func(a, b, lca framework.Sweep, args ...framework.Argument) framework.QueryResult {
		k := args[0].(int)
		t := treemap.Add(
			treemap.Subtract(a.(treemap.SummingMap), lca.(treemap.SummingMap)),
			treemap.Subtract(b.(treemap.SummingMap), lca.(treemap.SummingMap)))
		element, _ := t.FindByPrefixSum(k + 1)
		return element
	}
	return &pathKthElement{framework.NewPathQuery(t, initSweep, updateSweep, answerQuery)}
}
