// Package minority implements solution for range minority/majority problem and path minority/majority problem.
package minority

import (
	"bitbucket.org/sapalskimichal/persistent/examples/count"
	"bitbucket.org/sapalskimichal/persistent/examples/level_ancestor"
	"bitbucket.org/sapalskimichal/persistent/examples/set"
	"bitbucket.org/sapalskimichal/persistent/examples/sum"
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"fmt"
	"math/rand"
)

// RangeMinority represents a solution to range minority problem: given an array, preprocess it to quickly answer Minority queries.
type RangeMinority interface {
	// Minority returns any element that occurs strictly less than f * (b - a + 1) times in the range [a,b] of the underlying array or an error if such element doesn't exist.
	Minority(f float64, a, b int) (string, error)
}

// RangeMajority represents a solution to range majority problem: given an array, preprocess it to quickly answer Majority queries.
type RangeMajority interface {
	// Majority returns any element that occurs strictly more than f * (b - a + 1) times in the range [a,b] of the underlying array or an error if such element doesn't exist. Note that this method is probabilistic and can sometimes return an error even though a majority exists.
	Majority(f float64, a, b int) (string, error)
}

type rangeMinority struct {
	rangeSet   set.RangeSetSize
	rangeCount count.RangeElementCount
}

type rangeMajority struct {
	rangeCount count.RangeElementCount
	elements   []string
}

func (r rangeMinority) Minority(f float64, a, b int) (string, error) {
	n := b - a + 1
	k := 1 + int(1/f)
	for _, e := range r.rangeSet.KSetElements(k, a, b) {
		if float64(r.rangeCount.ElementCount(e, a, b)) < f*float64(n) {
			return e, nil
		}
	}
	return "", fmt.Errorf("No element in the range [%v,%v] occurs less than fraction %v of times.", a, b, f)
}

func (r rangeMajority) Majority(f float64, a, b int) (string, error) {
	n := b - a + 1
	k := 8 + 4*int(1/f)
	for i := 0; i < k; i++ {
		e := r.elements[a+rand.Intn(n)]
		if float64(r.rangeCount.ElementCount(e, a, b)) > f*float64(n) {
			return e, nil
		}
	}
	return "", fmt.Errorf("Probably no element in the range [%v,%v] occurs more than fraction %v of times.", a, b, f)
}

// NewRangeMinority returns a solution to the range minority problem for the given array.
func NewRangeMinority(elements []string) RangeMinority {
	return rangeMinority{set.NewRangeSetSize(elements), count.NewRangeElementCount(elements)}
}

// NewRangeMajority returns a solution to the range majority problem for the given array.
func NewRangeMajority(elements []string) RangeMajority {
	ce := make([]string, len(elements))
	copy(ce, elements)
	return rangeMajority{count.NewRangeElementCount(elements), ce}
}

// PathMinority represents a solution to path minority problem: given a tree, preprocess it to quickly answer Minority queries.
type PathMinority interface {
	// Minority returns any element that occurs strictly less than f * length times on the path from a to b in the underlying tree or an error if such element doesn't exist.
	Minority(f float64, a, b int) (string, error)
}

// PathMajority represents a solution to path majority problem: given a tree, preprocess it to quickly answer Majority queries.
type PathMajority interface {
	// Majority returns any element that occurs strictly less than f * length times on the path from a to b in the underlying tree or an error if such element doesn't exist.
	Majority(f float64, a, b int) (string, error)
}

type pathMinority struct {
	pathSet   set.PathSetElements
	pathCount count.PathElementCount
	pathSum   sum.PathSum
}

type pathMajority struct {
	pathCount     count.PathElementCount
	pathSum       sum.PathSum
	levelAncestor level_ancestor.LevelAncestor
	nodeById      map[int]*tree.Node
}

func (p pathMinority) Minority(f float64, a, b int) (string, error) {
	length := p.pathSum.Sum(a, b)
	k := 1 + int(1/f)
	for _, e := range p.pathSet.KSetElements(k, a, b) {
		if float64(p.pathCount.ElementCount(e, a, b)) < f*float64(length) {
			return e, nil
		}
	}
	return "", fmt.Errorf("No element on the path from %v to %v occurs less than fraction %v of times.", a, b, f)
}

func (p pathMajority) Majority(f float64, a, b int) (string, error) {
	length := p.pathSum.Sum(a, b)
	if length != 0 {
		aDepth := p.nodeById[a].Data.(int)
		bDepth := p.nodeById[b].Data.(int)
		lcaDepth := (aDepth + bDepth - length) / 2
		aLength := aDepth - lcaDepth
		k := 8 + 4*int(1/f)
		for i := 0; i < k; i++ {
			j := rand.Intn(length)
			d := 0
			v := a
			if j < aLength {
				d = lcaDepth + 1 + j
			} else {
				d = lcaDepth + 1 + (j - aLength)
				v = b
			}
			e := p.nodeById[p.levelAncestor.LevelAncestor(v, d)].ParentEdge.Data.(string)
			if float64(p.pathCount.ElementCount(e, a, b)) > f*float64(length) {
				return e, nil
			}
		}
	}
	return "", fmt.Errorf("No element on the path from %v to %v occurs more than fraction %v of times.", a, b, f)
}

func newPathLength(t tree.Tree) sum.PathSum {
	st := t.Copy()
	visit := func(v *tree.Node) {
		if v.ParentEdge != nil {
			v.ParentEdge.Data = 1
		}
	}
	st.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)
	return sum.NewPathSum(st)
}

// NewPathMinority returns a solution to the path minority problem for the given tree.
func NewPathMinority(t tree.Tree) PathMinority {
	return pathMinority{set.NewPathSetElements(t), count.NewPathElementCount(t), newPathLength(t)}
}

// NewPathMajority returns a solution to the path majority problem for the given tree.
func NewPathMajority(t tree.Tree) PathMajority {
	nodesById := map[int]*tree.Node{}
	ct := t.Copy()
	depth := 0
	enter := func(v *tree.Node) {
		nodesById[v.Id()] = v
		v.Data = depth
		depth++
	}
	exit := func(v *tree.Node) {
		depth--
	}
	ct.DFS(enter, tree.EmptyVisitor, exit)
	return pathMajority{count.NewPathElementCount(t), newPathLength(t), level_ancestor.NewLevelAncestor(ct), nodesById}
}
