package minority

import (
	"bitbucket.org/sapalskimichal/persistent/examples/count"
	"bitbucket.org/sapalskimichal/persistent/examples/set"
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"math"
	"math/rand"
	"strconv"
	"testing"
)

func countOccurrences(element string, elements []string, a, b int) int {
	res := 0
	for i, e := range elements {
		if a <= i && i <= b && e == element {
			res++
		}
	}
	return res
}

func TestRangeMinorityQuery(t *testing.T) {
	elements := []string{"a", "b", "c", "c", "b", "a", "c", "c", "c", "a", "b"}
	ds := NewRangeMinority(elements)

	if e, err := ds.Minority(0.3, 2, 9); err != nil || countOccurrences(e, elements, 2, 9) >= 3 {
		t.Errorf("(%v).Minority(0.3, 2, 9) = (%#v, %v) and occurs %v times, want (v, nil) where v occurs less than 3 times", elements, e, err, countOccurrences(e, elements, 2, 9))
	}
	if e, err := ds.Minority(0.2, 5, 9); err == nil {
		t.Errorf("(%v).Minority(0.2, 5, 9) = (%#v, %v) and %v occurs %v times, want (*, error)", elements, e, err, e, countOccurrences(e, elements, 2, 9))
	}
}

func TestRangeMajorityQuery(t *testing.T) {
	elements := []string{"a", "b", "c", "c", "b", "a", "c", "c", "c", "a", "b"}
	ds := NewRangeMajority(elements)

	if e, err := ds.Majority(0.5, 2, 9); err != nil || countOccurrences(e, elements, 2, 9) <= 4 {
		t.Errorf("(%v).Majority(0.5, 2, 9) = (%#v, %v) and occurs %v times, want (v, nil) where v occurs more than 4 times", elements, e, err, countOccurrences(e, elements, 2, 9))
	}
	if e, err := ds.Majority(0.4, 0, 2); err == nil {
		t.Errorf("(%v).Majority(0.4, 0, 2) = (%#v, %v) and %v occurs %v times, want (*, error)", elements, e, err, e, countOccurrences(e, elements, 0, 2))
	}
}

func hasRangeMinorityMajority(elements []string, f float64, a, b int) (minority bool, majority bool) {
	for i := a; i <= b; i++ {
		if float64(countOccurrences(elements[i], elements, a, b)) < f*float64(b-a+1) {
			minority = true
		}
		if float64(countOccurrences(elements[i], elements, a, b)) > f*float64(b-a+1) {
			majority = true
		}
	}
	return
}

func hasRangeMinority(elements []string, f float64, a, b int) bool {
	res, _ := hasRangeMinorityMajority(elements, f, a, b)
	return res
}

func hasRangeMajority(elements []string, f float64, a, b int) bool {
	_, res := hasRangeMinorityMajority(elements, f, a, b)
	return res
}

func randomElements(n, m int) []string {
	elements := make([]string, n, n)
	for i := 0; i < n; i++ {
		elements[i] = "e" + strconv.Itoa(rand.Intn(m))
	}
	return elements
}

func randomRangeMinority(n, m int) (RangeMinority, []string) {
	elements := randomElements(n, m)
	return NewRangeMinority(elements), elements
}

func randomRangeMajority(n, m int) (RangeMajority, []string) {
	elements := randomElements(n, m)
	return NewRangeMajority(elements), elements
}

func randomRange(n int) (a int, b int) {
	a, b = rand.Intn(n), rand.Intn(n)
	if a > b {
		a, b = b, a
	}
	return
}

func TestRandomRangeMinority(t *testing.T) {
	n, m := 30, 40
	for i := 1; i <= n; i++ {
		ds, elements := randomRangeMinority(i, 5)
		for j := 0; j < m; j++ {
			f := rand.Float64() * 1.1
			a, b := randomRange(i)
			e, err := ds.Minority(f, a, b)
			if err != nil && hasRangeMinority(elements, f, a, b) {
				t.Errorf("(%v).Minority(%v, %v, %v) = (%#v, %v), even though miniority exists.", elements, f, a, b, e, err)
			}
			if err == nil && float64(countOccurrences(e, elements, a, b)) >= f*float64(b-a+1) {
				t.Errorf("(%v).Minority(%v, %v, %v) = (%#v, %v), and %v occurs %v times, want less than %v times", elements, f, a, b, e, err, e, countOccurrences(e, elements, a, b), int(f*float64(b-a+1)))
			}
		}
	}
}

func TestRandomRangeMajority(t *testing.T) {
	n, m := 30, 40
	for i := 1; i <= n; i++ {
		ds, elements := randomRangeMajority(i, 5)
		for j := 0; j < m; j++ {
			f := rand.Float64()
			a, b := randomRange(i)
			e, err := ds.Majority(f, a, b)
			if err != nil && hasRangeMajority(elements, f, a, b) {
				t.Errorf("(%v).Majority(%v, %v, %v) = (%#v, %v), even though majority exists.", elements, f, a, b, e, err)
			}
			if err == nil && float64(countOccurrences(e, elements, a, b)) <= f*float64(b-a+1) {
				t.Errorf("(%v).Majority(%v, %v, %v) = (%#v, %v), and %v occurs %v times, want more than %v times", elements, f, a, b, e, err, e, countOccurrences(e, elements, a, b), int(f*float64(b-a+1)))
			}
		}
	}
}

func BenchmarkRangeMinority(b *testing.B) {
	n, m, f := 100000, 100, 0.2
	ds, _ := randomRangeMinority(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		ds.Minority(f, a, b)
	}
}

func BenchmarkRangeMajority(b *testing.B) {
	n, m, f := 100000, 100, 0.2
	ds, _ := randomRangeMajority(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		ds.Majority(f, a, b)
	}
}

func countPathOccurrences(element string, t tree.Tree, a, b int) int {
	return count.NewPathElementCount(t).ElementCount(element, a, b)
}

func TestPathMinority(t *testing.T) {
	n := tree.NewNode
	c := tree.NewChild
	tree := tree.Tree{n(1, "A", c("b", 2, "B", c("a", 3, "A"), c("c", 4, "C")), c("b", 5, "B", c("c", 6, "C")))}
	ds := NewPathMinority(tree)
	tests := []struct {
		f           float64
		a, b        int
		hasMinority bool
	}{
		{1.1, 1, 1, false},
		{1.1, 4, 3, true},
		{1.0, 2, 5, false},
		{0.7, 1, 6, true},
	}

	for i, test := range tests {
		if result, err := ds.Minority(test.f, test.a, test.b); (err == nil) != test.hasMinority || (test.hasMinority && float64(countPathOccurrences(result, tree, test.a, test.b)) >= test.f*float64(pathLength(tree, test.a, test.b))) {
			t.Errorf("Test %v: Minority(%v, %v, %v) = (%#v, %v), %v occurs %v times (want less than %v). Minority exists on this path: %v",
				i, test.f, test.a, test.b, result, err,
				result, countPathOccurrences(result, tree, test.a, test.b),
				int(math.Ceil(test.f*float64(pathLength(tree, test.a, test.b)))),
				test.hasMinority)
		}
	}
}

func TestPathMajority(t *testing.T) {
	n := tree.NewNode
	c := tree.NewChild
	tree := tree.Tree{n(1, "A", c("b", 2, "B", c("a", 3, "A"), c("c", 4, "C")), c("b", 5, "B", c("c", 6, "C")))}
	ds := NewPathMajority(tree)
	tests := []struct {
		f           float64
		a, b        int
		hasMajority bool
	}{
		{0.0, 1, 1, false},
		{0.0, 4, 3, true},
		{0.5, 6, 1, false},
		{0.4, 1, 6, true},
	}

	for i, test := range tests {
		if result, err := ds.Majority(test.f, test.a, test.b); (err == nil) != test.hasMajority || (test.hasMajority && float64(countPathOccurrences(result, tree, test.a, test.b)) <= test.f*float64(pathLength(tree, test.a, test.b))) {
			t.Errorf("Test %v: Majority(%v, %v, %v) = (%#v, %v), %v occurs %v times (want more than %v). Minority exists on this path: %v",
				i, test.f, test.a, test.b, result, err,
				result, countPathOccurrences(result, tree, test.a, test.b),
				int(math.Ceil(test.f*float64(pathLength(tree, test.a, test.b)))),
				test.hasMajority)
		}
	}
}

func randomTree(n, m int) tree.Tree {
	depth := int(math.Sqrt(float64(n)) + 2)
	t := tree.RandomTree(n, depth)
	visit := func(node *tree.Node) {
		if node.ParentEdge != nil {
			node.ParentEdge.Data = "e" + strconv.Itoa(rand.Intn(m))
		}
	}
	t.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)
	return t
}

func randomPathMinority(n, m int) (PathMinority, tree.Tree) {
	t := randomTree(n, m)
	return NewPathMinority(t), t
}

func randomPathMajority(n, m int) (PathMajority, tree.Tree) {
	t := randomTree(n, m)
	return NewPathMajority(t), t
}

func pathLength(t tree.Tree, a, b int) int {
	res := 0
	exit := func(v *tree.Node) {
		if a == b {
			return
		}
		if v.Id() == a {
			res++
			a = v.ParentEdge.Parent.Id()
		}
		if v.Id() == b {
			res++
			b = v.ParentEdge.Parent.Id()
		}
	}
	t.DFS(tree.EmptyVisitor, tree.EmptyVisitor, exit)
	return res
}

func hasPathMinority(t tree.Tree, f float64, a, b int) bool {
	for _, e := range set.NewPathSetElements(t).SetElements(a, b) {
		if float64(countPathOccurrences(e, t, a, b)) < f*float64(pathLength(t, a, b)) {
			return true
		}
	}
	return false
}

func hasPathMajority(t tree.Tree, f float64, a, b int) bool {
	for _, e := range set.NewPathSetElements(t).SetElements(a, b) {
		if float64(countPathOccurrences(e, t, a, b)) > f*float64(pathLength(t, a, b)) {
			return true
		}
	}
	return false
}

func TestRandomPathMinority(t *testing.T) {
	n, m := 30, 40
	for i := 1; i <= n; i++ {
		ds, tree := randomPathMinority(i, 5)
		for j := 0; j < m; j++ {
			a, b := rand.Intn(i), rand.Intn(i)
			f := rand.Float64() * 1.1
			result, err := ds.Minority(f, a, b)
			if err != nil && hasPathMinority(tree, f, a, b) {
				t.Errorf("Minority(%v, %v, %v) = (%#v, %v), even though miniority exists.", f, a, b, result, err)
			}
			if err == nil && float64(countPathOccurrences(result, tree, a, b)) >= f*float64(pathLength(tree, a, b)) {
				t.Errorf("Minority(%v, %v, %v) = (%#v, %v), and %v occurs %v times, want less than %v times", f, a, b, result, err, result, countPathOccurrences(result, tree, a, b), int(pathLength(tree, a, b)))
			}
		}
	}
}

func TestRandomPathMajority(t *testing.T) {
	n, m := 30, 40
	for i := 1; i <= n; i++ {
		ds, tree := randomPathMajority(i, 5)
		for j := 0; j < m; j++ {
			a, b := rand.Intn(i), rand.Intn(i)
			f := rand.Float64()
			result, err := ds.Majority(f, a, b)
			if err != nil && hasPathMajority(tree, f, a, b) {
				t.Errorf("Majority(%v, %v, %v) = (%#v, %v), even though majority exists.", f, a, b, result, err)
			}
			if err == nil && float64(countPathOccurrences(result, tree, a, b)) <= f*float64(pathLength(tree, a, b)) {
				t.Errorf("Majority(%v, %v, %v) = (%#v, %v), and %v occurs %v times, want more than %v times", f, a, b, result, err, result, countPathOccurrences(result, tree, a, b), int(pathLength(tree, a, b)))
			}
		}
	}
}

func BenchmarkPathMinority(b *testing.B) {
	n, m, f := 100000, 100, 0.2
	ds, _ := randomPathMinority(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		ds.Minority(f, a, b)
	}
}

func BenchmarkPathMajority(b *testing.B) {
	n, m, f := 100000, 100, 0.1
	ds, _ := randomPathMajority(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		ds.Majority(f, a, b)
	}
}
