package forest

import (
	"math/rand"
	"sort"
	"testing"
)

func TestMinimumSpanningForest(t *testing.T) {
	v1, v2, v3, v4 := Vertex(1), Vertex(2), Vertex(3), Vertex(4)
	g := Graph{
		[]Vertex{v1, v2, v3, v4},
		[]Edge{{v1, v2, 10}, {v2, v3, 3}, {v4, v1, 5}, {v1, v3, 4}},
	}
	msf := NewMinimumSpanningForest(g)

	if w := msf.RangeMinimumSpanningForestWeight(1, 4); w != 7 {
		t.Errorf("RangeMinimumSpanningForestWeight(1, 4) = %v, want 7", w)
	}

	if w := msf.RangeMinimumSpanningForestWeight(4, 10); w != 19 {
		t.Errorf("RangeMinimumSpanningForestWeight(4, 10) = %v, want 19", w)
	}

	if w := msf.RangeMinimumSpanningForestWeight(4, 9); w != 9 {
		t.Errorf("RangeMinimumSpanningForestWeight(4, 9) = %v, want 9", w)
	}

	if w := msf.RangeMinimumSpanningForestWeight(1, 10); w != 12 {
		t.Errorf("RangeMinimumSpanningForestWeight(1, 10) = %v, want 12", w)
	}

	if w := msf.RangeMinimumSpanningForestWeight(11, 20); w != 0 {
		t.Errorf("RangeMinimumSpanningForestWeight(11, 20) = %v, want 0", w)
	}

	if w := msf.RangeMinimumSpanningForestWeight(1, 2); w != 0 {
		t.Errorf("RangeMinimumSpanningForestWeight(1, 2) = %v, want 0", w)
	}
}

func TestMultiedges(t *testing.T) {
	v1, v2 := Vertex(1), Vertex(2)
	g := Graph{
		[]Vertex{v1, v2},
		[]Edge{{v1, v2, 10}, {v2, v1, 3}},
	}
	msf := NewMinimumSpanningForest(g)

	if w := msf.RangeMinimumSpanningForestWeight(2, 9); w != 3 {
		t.Errorf("RangeMinimumSpanningForestWeight(2, 9) = %v, want 3", w)
	}
	if w := msf.RangeMinimumSpanningForestWeight(5, 19); w != 10 {
		t.Errorf("RangeMinimumSpanningForestWeight(5, 19) = %v, want 10", w)
	}
}

type byWeightAscending []Edge

func (a byWeightAscending) Len() int           { return len(a) }
func (a byWeightAscending) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byWeightAscending) Less(i, j int) bool { return a[i].Weight < a[j].Weight }

func randomEdge(n int) Edge {
	a, b := rand.Intn(n), rand.Intn(n)
	for a == b {
		b = rand.Intn(n)
	}
	return Edge{Vertex(a), Vertex(b), rand.Intn(100)}
}

func randomGraph(n, m int) Graph {
	verts := make([]Vertex, n)
	for i := 0; i < n; i++ {
		verts[i] = Vertex(i)
	}
	edges := make([]Edge, m)
	for i := 0; i < m; i++ {
		edges[i] = randomEdge(n)
	}
	return Graph{verts, edges}
}

func randomWeightRange() (int, int) {
	a, b := rand.Intn(100), rand.Intn(100)
	if a > b {
		a, b = b, a
	}
	return a, b
}

func bruteForceRMSFW(g Graph, a, b int, t *testing.T) int {
	edges := make([]Edge, 0)
	component := make(map[Vertex]int)
	for i, v := range g.Vertices {
		component[v] = i
	}
	for _, e := range g.Edges {
		if a <= e.Weight && e.Weight <= b {
			edges = append(edges, e)
		}
	}
	sort.Sort(byWeightAscending(edges))
	res := 0
	for _, e := range edges {
		if component[e.A] == component[e.B] {
			continue
		}
		res += e.Weight
		bc := component[e.B]
		for v, c := range component {
			if c == bc {
				component[v] = component[e.A]
			}
		}
	}
	return res
}

func TestMinimumSpanningForestRandom(t *testing.T) {
	n, m, q := 30, 100, 100
	for i := 0; i < m; i++ {
		g := randomGraph(n, i)
		msf := NewMinimumSpanningForest(g)
		for j := 0; j < q; j++ {
			a, b := randomWeightRange()
			if w, bw := msf.RangeMinimumSpanningForestWeight(a, b), bruteForceRMSFW(g, a, b, t); w != bw {
				t.Errorf("RangeMinimumSpanningForestWeight(%v, %v) = %v, want %v", a, b, w, bw)
			}
		}
	}
}

func BenchmarkMinimumSpanningForest(b *testing.B) {
	n, m := 30000, 100000
	g := randomGraph(n, m)
	msf := NewMinimumSpanningForest(g)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		wa, wb := randomWeightRange()
		msf.RangeMinimumSpanningForestWeight(wa, wb)
	}
}
