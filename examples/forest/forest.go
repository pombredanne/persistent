// forest package implements solution to the range minimum spanning forest problem: given a graph preprocess it to quickly answer range minimum spanning forest queries: what is the weight of the minimum spanning forest consisting only of edges with weight from the range [a,b].
package forest

import (
	"bitbucket.org/sapalskimichal/persistent/examples/framework"
	"bitbucket.org/sapalskimichal/persistent/examples/lctree"
	"bitbucket.org/sapalskimichal/persistent/treemap"
	"sort"
)

// Vertex represents a vertex in a graph.
type Vertex int

// Edge represents an undirected edge between A and B in a graph.
type Edge struct {
	A, B   Vertex
	Weight int
}

// MinWeight is the minimum possible weight.
var MinWeight = -1

// Graph represents an undirected graph with no loops. Multiple edges between the same pair of vertices are possible.
type Graph struct {
	Vertices []Vertex
	Edges    []Edge
}

// MinimumSpanningForest represents to range minimum spanning forest problem.
type MinimumSpanningForest interface {
	// RangeMinimumSpanningForestWeight returns the weight of minimum spanning forest which consists only of edges with weight in the range [a,b].
	RangeMinimumSpanningForestWeight(a, b int) int
}

type minimumSpanningForest struct {
	edges      []Edge
	rangeQuery framework.RangeQuery
}

func (m *minimumSpanningForest) findBegin(weight int) int {
	beg, end := -1, len(m.edges)
	for end-beg > 1 {
		mid := (beg + end) / 2
		if m.edges[mid].Weight <= weight {
			end = mid
		} else {
			beg = mid
		}
	}
	return end
}

func (m *minimumSpanningForest) findEnd(weight int) int {
	beg, end := -1, len(m.edges)
	for end-beg > 1 {
		mid := (beg + end) / 2
		if m.edges[mid].Weight < weight {
			end = mid
		} else {
			beg = mid
		}
	}
	return beg
}

func (m *minimumSpanningForest) RangeMinimumSpanningForestWeight(a, b int) int {
	beg, end := m.findBegin(b), m.findEnd(a)
	if beg > end {
		return 0
	}
	return m.rangeQuery.Query(end, end, beg).(int)
}

type byWeightDescending []Edge

func (a byWeightDescending) Len() int           { return len(a) }
func (a byWeightDescending) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byWeightDescending) Less(i, j int) bool { return a[i].Weight > a[j].Weight }

type edgeInfo struct {
	idx    int
	weight int
}

type edgeInfoComparator int

func (edgeInfoComparator) Less(w1, w2 lctree.Weight) bool {
	return w1.(edgeInfo).weight < w2.(edgeInfo).weight
}

// NewMinimumSpanningForest constructs a solution to the range minimum spanning forest problem for the given graph. The graph is not modified.
func NewMinimumSpanningForest(g Graph) MinimumSpanningForest {
	edges := make([]Edge, len(g.Edges))
	copy(edges, g.Edges)
	sort.Sort(byWeightDescending(edges))

	elements := make([]framework.Element, len(edges))
	lc := lctree.NewLinkCut(edgeInfoComparator(0))
	lcNodes := make(map[Vertex]lctree.NodeId)
	vertices := make(map[lctree.NodeId]Vertex)
	edgeMap := make(map[int]int)

	for _, v := range g.Vertices {
		lcNodes[v] = lc.NewNode()
		vertices[lcNodes[v]] = v
	}
	for i, e := range edges {
		edgeMap[i] = 0
		elements[i] = e
	}
	idx := 0
	initSweep := func([]framework.Element) framework.Sweep {
		return treemap.NewStaticIntSummingMap(edgeMap)
	}
	updateSweep := func(e framework.Element, s framework.Sweep) framework.Sweep {
		m := s.(treemap.SummingMap)
		ed := e.(Edge)
		a, b := lcNodes[ed.A], lcNodes[ed.B]
		lc.Evert(a)
		if lc.Root(b) == a {
			x, w := lc.MaxParentLink(b)
			edgeIdx := w.(edgeInfo).idx
			m, _ = m.UpdateSumming(edgeIdx, 0)
			lc.Cut(x)
		}
		lc.Link(a, b, lctree.Weight(edgeInfo{idx, ed.Weight}))
		m, _ = m.UpdateSumming(idx, ed.Weight)
		idx++
		return m
	}
	answerQuery := func(aSweep, bSweep framework.Sweep, args ...framework.Argument) framework.QueryResult {
		a := args[0]
		m := bSweep.(treemap.SummingMap)
		return m.Sum(a, len(edges))
	}
	return &minimumSpanningForest{edges, framework.NewRangeQuery(elements, initSweep, updateSweep, answerQuery)}
}
