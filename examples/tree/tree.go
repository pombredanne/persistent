// Package tree implements a rooted tree structure, to be used by other examples.
package tree

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
)

// Data type represents data that can be stored with nodes or edges.
type Data interface{}

// Node structure represents a single node in the tree.
type Node struct {
	id int
	// ChildrenEdges stores edges to children of this node.
	ChildrenEdges []*Edge
	// Data represents additional information stored in this node.
	Data Data
	// ParentEdge is the edge to the parent of this node or nil if its the root.
	ParentEdge *Edge
}

// Edge structure represents a single edge in the tree. Edges are undirected
type Edge struct {
	// Parent and Child are the ends of the edge.
	Parent, Child *Node
	// Data represents additional information stored with this edge.
	Data Data
}

// NewNode returns a new Node with a given id, data and children.
func NewNode(id int, data Data, childrenEdges ...*Edge) *Node {
	n := &Node{id, []*Edge{}, data, nil}
	for _, e := range childrenEdges {
		n.AppendChild(e)
	}
	return n
}

// NewChildEdge returns a new edge with Parent unfilled - it will be filled when this edge is passed to NewNode or Node.AppendChild functions.
func NewChildEdge(to *Node, data Data) *Edge {
	return &Edge{nil, to, data}
}

// NewChild is a convenience function for creating new child along with its parent edge.
func NewChild(edgeData Data, id int, nodeData Data, childrenEdges ...*Edge) *Edge {
	return NewChildEdge(NewNode(id, nodeData, childrenEdges...), edgeData)
}

// Id returns this node's identifier.
func (n *Node) Id() int {
	return n.id
}

// Children returns number of children of this node.
func (n *Node) Children() int {
	return len(n.ChildrenEdges)
}

// Child(i) returns a pointer to i-th child of this node.
func (n *Node) Child(i int) *Node {
	return n.ChildrenEdges[i].Child
}

// AppendChild(child) adds new child to the node.
// Newly added child will be at the end of children list.
func (n *Node) AppendChild(childEdge *Edge) {
	childEdge.Parent = n
	childEdge.Child.ParentEdge = childEdge
	n.ChildrenEdges = append(n.ChildrenEdges, childEdge)
}

// Tree structure represents a rooted tree.
type Tree struct {
	// Root is a pointer to the root of this tree.
	Root *Node
}

func copySubtree(v *Node) *Node {
	n := &Node{}
	n.id = v.id
	for _, e := range v.ChildrenEdges {
		n.AppendChild(NewChildEdge(copySubtree(e.Child), e.Data))
	}
	n.Data = v.Data
	return n
}

// Copy returns a copy of the tree.
func (t Tree) Copy() Tree {
	return Tree{copySubtree(t.Root)}
}

func (n *Node) graphvizString(printData bool) string {
	if !printData {
		return fmt.Sprintf("\"%v\"", n.id)
	}
	quotedData := fmt.Sprintf("%q", fmt.Sprintf("%+v", n.Data))
	return fmt.Sprintf("\"%v: '%s'\"", n.id, strings.Trim(quotedData, "\""))
}

func (e *Edge) graphvizString(printData bool) string {
	if !printData {
		return ""
	}
	quotedData := fmt.Sprintf("%q", fmt.Sprintf("%+v", e.Data))
	return fmt.Sprintf(" [label=\"'%s'\"]", strings.Trim(quotedData, "\""))
}

func (n *Node) graphvizSubtree(buffer *bytes.Buffer, printData bool) {
	for _, e := range n.ChildrenEdges {
		buffer.WriteString(fmt.Sprintf("\t%s -- %s%s;\n", n.graphvizString(printData), e.Child.graphvizString(printData), e.graphvizString(printData)))
		e.Child.graphvizSubtree(buffer, printData)
	}
}

func (t Tree) graphvizString(printData bool) string {
	var buffer bytes.Buffer
	buffer.WriteString("graph T {\n")
	if t.Root != nil {
		if t.Root.Children() == 0 {
			buffer.WriteString(fmt.Sprintf("\tnode %s;\n", t.Root.graphvizString(printData)))
		} else {
			t.Root.graphvizSubtree(&buffer, printData)
		}
	}
	buffer.WriteString("}")
	return buffer.String()
}

// GraphvizString generates string representation of the tree in Graphviz format.
func (t Tree) GraphvizString() string {
	return t.graphvizString(true)
}

// GraphvizStringNoData generates string representation of the tree in Graphviz format, but doesn't print node data (just ids).
func (t Tree) GraphvizStringNoData() string {
	return t.graphvizString(false)
}

func (n *Node) depth() int {
	d := 1
	for _, e := range n.ChildrenEdges {
		cd := e.Child.depth() + 1
		if cd > d {
			d = cd
		}
	}
	return d
}

// Depth returns the depth of the tree.
// The depth is the maximum number of nodes on any path from root to a leaf.
func (t Tree) Depth() int {
	if t.Root == nil {
		return 0
	} else {
		return t.Root.depth()
	}
}

func (n *Node) size() int {
	s := 1
	for _, e := range n.ChildrenEdges {
		s += e.Child.size()
	}
	return s
}

// Size returns number of nodes in the tree.
// Running time is O(n).
func (t Tree) Size() int {
	if t.Root == nil {
		return 0
	} else {
		return t.Root.size()
	}
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func generatePath(length, id int) *Edge {
	node := &Node{id, make([]*Edge, 0, 1), struct{}{}, nil}
	if length > 1 {
		node.AppendChild(generatePath(length-1, id+1))
	}
	return NewChildEdge(node, struct{}{})
}

func (n *Node) randomWalk(steps int) *Node {
	if steps == 0 {
		return n
	}
	return n.ChildrenEdges[rand.Intn(n.Children())].Child.randomWalk(steps - 1)
}

// Visitor is a type of function used for visiting tree nodes.
type Visitor func(*Node)

// EmptyVisitor does nothning when visiting a node.
func EmptyVisitor(*Node) {}

// ParentVisitor is a type of function used for visiting tree nodes. It gets current node and it's parent (or nil for root node).
type ParentVisitor func(node *Node, parent *Node)

// EmptyParentVisitor does nothing when visiting a node.
func EmptyParentVisitor(*Node, *Node) {}

func (n *Node) parentDFS(enter, reenter, exit ParentVisitor, parent *Node) {
	enter(n, parent)
	for _, e := range n.ChildrenEdges {
		e.Child.parentDFS(enter, reenter, exit, n)
		reenter(n, parent)
	}
	exit(n, parent)
}

// DFS performs a deep-first search of the tree.
// enter, reenter and exit visitors are executed when the search enters the node for the first time,
// returns (from a child) to previously visited node or exits the node. They may modify node data.
func (t Tree) DFS(enter, reenter, exit Visitor) {
	parentEnter := func(v, _ *Node) {
		enter(v)
	}
	parentReenter := func(v, _ *Node) {
		reenter(v)
	}
	parentExit := func(v, _ *Node) {
		exit(v)
	}
	t.ParentDFS(parentEnter, parentReenter, parentExit)
}

func (t Tree) ParentDFS(enter, reenter, exit ParentVisitor) {
	if t.Root != nil {
		t.Root.parentDFS(enter, reenter, exit, nil)
	}
}

// RandomTree returns a random tree with specified number of nodes and depth.
// The algorithm works as follows:
//   - a path consisting of depth nodes is created
//   - the following process is repeated, until sufficient number of nodes is generated:
//      - choose a random integer l from range [1, depth-1]
//      - walk randomly down from the root, until you reach level l
//      - generate a path of length depth-l and attach it to the node on level l
// The algorithm stops when specified number of nodes is generated, so the generated tree will always have specified size.
// Nodes are numbered from 0 to size-1, according to the order in which they were generated.
// If it's impossible to generate such tree, an empty tree is returned.
func RandomTree(size, depth int) Tree {
	var tree Tree
	if size <= 0 || depth <= 0 {
		return tree
	}
	if depth == 1 && size != 1 {
		return tree
	}
	count := min(depth, size)
	tree.Root = generatePath(count, 0).Child
	for count < size {
		level := rand.Intn(depth-1) + 1
		node := tree.Root.randomWalk(level - 1)
		pathLength := min(size-count, depth-level)
		node.AppendChild(generatePath(pathLength, count))
		count += pathLength
	}
	return tree
}
