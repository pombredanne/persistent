package tree

import "testing"

func sampleTree() Tree {
	return Tree{
		NewNode(4, "Four",
			NewChild("4-2", 2, "Two", NewChild("2-5", 5, "Five")),
			NewChild("4-3", 3, "Three", NewChild("3-1", 1, "One", NewChild("1-0", 0, "Zero"))))}
}

func TestTreeFromParents(t *testing.T) {
	tree := sampleTree()
	r := tree.Root
	if id := r.Id(); id != 4 {
		t.Errorf("Tree root = %v, want %v", id, 4)
	}
	if r.Data != "Four" {
		t.Errorf("Tree root data = %v, want \"Four\"", r.Data)
	}
	if c := r.Children(); c != 2 {
		t.Errorf("Root.Children() = %v, want %v", c, 2)
	}
	if id := r.Child(0).Id(); id != 2 {
		t.Errorf("First child of root = %v, want %v", id, 2)
	}
	if id := r.Child(1).Id(); id != 3 {
		t.Errorf("Second child of root = %v, want %v", id, 3)
	}
}

func TestNodeAppendChild(t *testing.T) {
	tree := sampleTree()
	tree.Root.AppendChild(NewChild("-6", 6, "Six"))
	if id := tree.Root.Child(2).Id(); id != 6 {
		t.Errorf("New child of root = %v, want %v", id, 6)
	}
}

func TestGraphvizOneNode(t *testing.T) {
	tree := Tree{NewNode(0, struct{ Foo string }{"bar"})}
	graphvizString := `graph T {
	node "0: '{Foo:bar}'";
}`
	if s := tree.GraphvizString(); s != graphvizString {
		t.Errorf("GraphvizString() = %v, want %v", s, graphvizString)
	}
	graphvizStringNoData := `graph T {
	node "0";
}`
	if s := tree.GraphvizStringNoData(); s != graphvizStringNoData {
		t.Errorf("GraphvizString() = %v, want %v", s, graphvizStringNoData)
	}
}

func TestGraphvizSampleTree(t *testing.T) {
	tree := sampleTree()
	graphvizString := `graph T {
	"4: 'Four'" -- "2: 'Two'" [label="'4-2'"];
	"2: 'Two'" -- "5: 'Five'" [label="'2-5'"];
	"4: 'Four'" -- "3: 'Three'" [label="'4-3'"];
	"3: 'Three'" -- "1: 'One'" [label="'3-1'"];
	"1: 'One'" -- "0: 'Zero'" [label="'1-0'"];
}`
	if s := tree.GraphvizString(); s != graphvizString {
		t.Errorf("GraphvizString() = %v, want %v", s, graphvizString)
	}

	graphvizString = `graph T {
	"4" -- "2";
	"2" -- "5";
	"4" -- "3";
	"3" -- "1";
	"1" -- "0";
}`
	if s := tree.GraphvizStringNoData(); s != graphvizString {
		t.Errorf("GraphvizString() = %v, want %v", s, graphvizString)
	}
}

func TestDepth(t *testing.T) {
	var tree Tree
	if d := tree.Depth(); d != 0 {
		t.Errorf("Depth() = %v, want %v", d, 0)
	}
	if d := sampleTree().Depth(); d != 4 {
		t.Errorf("Depth() = %v, want %v", d, 4)
	}
}

func TestSize(t *testing.T) {
	var tree Tree
	if s := tree.Size(); s != 0 {
		t.Errorf("Size() = %v, want %v", s, 0)
	}
	if s := sampleTree().Size(); s != 6 {
		t.Errorf("Size() = %v, want %v", s, 6)
	}
}

func TestRandomTree(t *testing.T) {
	tree := RandomTree(10, 4)
	if s := tree.Size(); s != 10 {
		t.Errorf("Size() = %v, want %v", s, 10)
	}
	if d := tree.Depth(); d != 4 {
		t.Errorf("Depth() = %v, want %v", d, 4)
	}

	if r := RandomTree(0, 0); r.Root != nil {
		t.Errorf("Invalid arguments, result = %v, want empty tree.", r)
	}

	if r := RandomTree(2, 1); r.Root != nil {
		t.Errorf("Invalid arguments, result = %v, want empty tree.", r)
	}
}

func slicesEqual(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestDFS(t *testing.T) {
	tree := sampleTree()
	verts := make([]int, 0, 2*tree.Size())
	visit := func(v *Node) {
		verts = append(verts, v.Id())
	}
	exit := func(v *Node) {
		verts = append(verts, -1)
	}
	tree.DFS(visit, visit, exit)
	expectedVerts := []int{4, 2, 5, -1, 2, -1, 4, 3, 1, 0, -1, 1, -1, 3, -1, 4, -1}
	if !slicesEqual(verts, expectedVerts) {
		t.Errorf("DFS() visit pattern = %v, want %v", verts, expectedVerts)
	}
}

func TestParentDFS(t *testing.T) {
	tree := sampleTree()
	verts := make([]int, 0, 2*tree.Size())
	noParentReenter := 0
	noParentExit := 0
	visit := func(v, p *Node) {
		pId := -1
		if p != nil {
			pId = p.Id()
		}
		verts = append(verts, v.Id(), pId)
	}
	reenter := func(v, p *Node) {
		if p == nil {
			noParentReenter++
		}
	}
	exit := func(v, p *Node) {
		if p == nil {
			noParentExit++
		}
	}
	tree.ParentDFS(visit, reenter, exit)
	expectedVerts := []int{4, -1, 2, 4, 5, 2, 3, 4, 1, 3, 0, 1}
	if !slicesEqual(verts, expectedVerts) {
		t.Errorf("DFS() visit pattern = %v, want %v", verts, expectedVerts)
	}
	if noParentReenter != 2 {
		t.Errorf("noParentReenter = %v, want 2", noParentReenter)
	}
	if noParentExit != 1 {
		t.Errorf("noParentExit = %v, want 1", noParentExit)
	}
}

func TestCopy(t *testing.T) {
	verts := make([]int, 0, 12)
	tree := sampleTree()
	copy := tree.Copy()
	n := 0
	generateData := func(v *Node) {
		v.Data = n
		n++
	}
	visit := func(v *Node) {
		verts = append(verts, v.Data.(int))
	}
	tree.DFS(generateData, EmptyVisitor, EmptyVisitor)
	copy.DFS(generateData, EmptyVisitor, EmptyVisitor)
	tree.DFS(visit, EmptyVisitor, EmptyVisitor)
	copy.DFS(visit, EmptyVisitor, EmptyVisitor)
	expectedVerts := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
	if !slicesEqual(verts, expectedVerts) {
		t.Errorf("DFS() visit on tree and copy = %v, want %v", verts, expectedVerts)
	}
}
