package tree

import (
	"math/rand"
	"testing"
)

func sampleLCA() LCA {
	return BuildLCA(sampleTree())
}

func TestLCASize(t *testing.T) {
	lcaTree := sampleLCA()
	if s := lcaTree.Size(); s != 6 {
		t.Errorf("Size() = %v, want %v", s, 6)
	}
}

func TestNodeById(t *testing.T) {
	lcaTree := sampleLCA()
	if node := lcaTree.NodeById(100); node != nil {
		t.Errorf("NodeById(100) = %v, want nil", node)
	}
	if data := lcaTree.NodeById(5).Data; data != "Five" {
		t.Errorf("NodeById(5).Data = %v, want \"Five\"", data)
	}
}

func TestLCA(t *testing.T) {
	lcaTree := sampleLCA()
	tests := []struct{ node1, node2, result int }{
		{3, 3, 3},
		{0, 5, 4},
		{2, 3, 4},
		{5, 2, 2},
	}
	for _, test := range tests {
		if id := lcaTree.LCA(test.node1, test.node2).Id(); id != test.result {
			t.Errorf("LCA(%v, %v) = %v, want %v", test.node1, test.node2, id, test.result)
		}
	}
}

func BenchmarkLCA(b *testing.B) {
	n, depth := 1000000, 10000
	tree := BuildLCA(RandomTree(n, depth))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		tree.LCA(a, b)
	}
}
