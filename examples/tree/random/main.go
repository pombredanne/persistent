// Generate random tree.
package main

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	size, depth := 10, 4
	if len(os.Args) > 1 {
		if s, err := strconv.Atoi(os.Args[1]); err == nil {
			size = s
		}
	}
	if len(os.Args) > 2 {
		if d, err := strconv.Atoi(os.Args[2]); err == nil {
			depth = d
		}
	}
	fmt.Println(tree.RandomTree(size, depth).GraphvizStringNoData())
}
