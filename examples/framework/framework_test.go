package framework

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"testing"
)

func TestRangeQuery(t *testing.T) {
	elemLen := 0

	initSweep := func(elements []Element) Sweep {
		elemLen = len(elements)
		return 0
	}
	updateSweep := func(element Element, sweep Sweep) Sweep {
		if element.(int) > 0 {
			return 1 + sweep.(int)
		} else {
			return sweep.(int)
		}
	}
	answerQuery := func(begSweep, endSweep Sweep, args ...Argument) QueryResult {
		return endSweep.(int) - begSweep.(int) + args[0].(int)
	}
	elements := []Element{5, 2, -7, 10, 2}
	rangeQuery := NewRangeQuery(elements, initSweep, updateSweep, answerQuery)
	if elemLen != len(elements) {
		t.Errorf("Length of elements from initSweep is %v, want %v", elemLen, len(elements))
	}

	if result := rangeQuery.Query(1, 1, 100); result.(int) != 101 {
		t.Errorf("Query(1, 1, 100) = %v, want %v", result, 101)
	}

	if result := rangeQuery.Query(1, 3, 100); result.(int) != 102 {
		t.Errorf("Query(1, 3, 100) = %v, want %v", result, 102)
	}

	panicked := false
	defer func() {
		if r := recover(); r != nil {
			panicked = true
		}
	}()
	rangeQuery.Query(3, 2, 10)
	if !panicked {
		t.Error("Not panicked with invalid range: [3, 2]")
	}

	panicked = false
	defer func() {
		if r := recover(); r != nil {
			panicked = true
		}
	}()
	rangeQuery.Query(4, 5, 10)
	if !panicked {
		t.Error("Not panicked with invalid range: [4, 5]")
	}
}

func TestPathQuery(t *testing.T) {
	treeSize := 0

	initSweep := func(t tree.Tree) Sweep {
		treeSize = t.Size()
		return 0
	}
	updateSweep := func(n *tree.Node, sweep Sweep) Sweep {
		if n.ParentEdge.Data.(int) > 0 {
			return 1 + sweep.(int)
		} else {
			return sweep.(int)
		}
	}
	answerQuery := func(a, b, lca Sweep, args ...Argument) QueryResult {
		return a.(int) + b.(int) - 2*lca.(int) + args[0].(int)
	}

	n := tree.NewNode
	c := tree.NewChild
	tree := tree.Tree{n(1, 5, c(-7, 2, "-7", c(10, 3, "10"), c(-20, 4, "-20")), c(3, 5, "3"), c(7, 6, "7"))}

	pathQuery := NewPathQuery(tree, initSweep, updateSweep, answerQuery)
	if treeSize != tree.Size() {
		t.Errorf("Size of tree from initSweep is %v, want %v", treeSize, tree.Size())
	}

	if result := pathQuery.Query(1, 1, 100); result.(int) != 100 {
		t.Errorf("Query(1, 1, 100) = %v, want %v", result, 100)
	}

	if result := pathQuery.Query(5, 4, 200); result.(int) != 201 {
		t.Errorf("Query(5, 4, 200) = %v, want %v", result, 201)
	}

	if result := pathQuery.Query(5, 3, 200); result.(int) != 202 {
		t.Errorf("Query(5, 3, 200) = %v, want %v", result, 202)
	}
}
