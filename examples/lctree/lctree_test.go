package lctree

import (
	"math"
	"math/rand"
	"testing"
)

var minWeight = int(math.MinInt32)

type intWeightComparator int

func (intWeightComparator) Less(w1, w2 Weight) bool {
	return w1.(int) < w2.(int)
}

var comparator = intWeightComparator(0)

type simpleNode struct {
	id           NodeId
	parent       *simpleNode
	parentWeight Weight
}

type simpleLinkCut struct {
	nodes  map[NodeId]*simpleNode
	lastId NodeId
}

func (lc *simpleLinkCut) NewNode() NodeId {
	for lc.nodes[lc.lastId] != nil {
		lc.lastId++
	}
	return lc.getSimpleNode(lc.lastId).id
}

func (lc *simpleLinkCut) getSimpleNode(v NodeId) *simpleNode {
	if lc.nodes[v] == nil {
		lc.nodes[v] = &simpleNode{v, nil, minWeight}
	}
	return lc.nodes[v]
}

func (lc *simpleLinkCut) Root(v NodeId) NodeId {
	vn := lc.getSimpleNode(v)
	for vn.parent != nil {
		vn = vn.parent
	}
	return vn.id
}

func (lc *simpleLinkCut) Evert(v NodeId) {
	vn := lc.getSimpleNode(v)
	if vn.parent == nil {
		return
	}
	lc.Evert(vn.parent.id)
	vn.parent.parent = vn
	vn.parent.parentWeight = vn.parentWeight
	lc.Cut(v)
}

func (lc *simpleLinkCut) Link(v, u NodeId, weight Weight) {
	if lc.Root(v) == lc.Root(u) {
		return
	}
	lc.Evert(v)
	vn := lc.getSimpleNode(v)
	un := lc.getSimpleNode(u)
	vn.parent = un
	vn.parentWeight = weight
}

func (lc *simpleLinkCut) Cut(v NodeId) {
	vn := lc.getSimpleNode(v)
	vn.parent = nil
	vn.parentWeight = minWeight
}

func (lc *simpleLinkCut) MaxParentLink(v NodeId) (u NodeId, weight Weight) {
	vn := lc.getSimpleNode(v)
	if vn.parent == nil {
		return v, nil
	}
	u, weight = vn.id, vn.parentWeight
	for vn.parent != nil {
		if comparator.Less(weight, vn.parentWeight) {
			u, weight = vn.id, vn.parentWeight
		}
		vn = vn.parent
	}
	return
}

func (lc *simpleLinkCut) ParentWeight(v NodeId) Weight {
	vn := lc.getSimpleNode(v)
	if vn.parent == nil {
		return nil
	}
	return vn.parentWeight
}

func newSimpleLinkCut() *simpleLinkCut {
	return &simpleLinkCut{make(map[NodeId]*simpleNode), 0}
}

func TestLinkCut(t *testing.T) {
	return
	lc := NewLinkCut(comparator)
	v1 := lc.NewNode()
	v2 := lc.NewNode()
	v3 := lc.NewNode()

	lc.Link(v3, v2, Weight(10))
	lc.Link(v2, v1, Weight(11))

	if id, w := lc.MaxParentLink(v2); id != v2 || w != Weight(11) {
		t.Errorf("MaxParentLink(%v) = (%v, %v), want (%v, 11)", v2, id, w, v2)
	}

	if id, w := lc.MaxParentLink(v3); id != v2 || w != Weight(11) {
		t.Errorf("MaxParentLink(%v) = (%v, %v), want (%v, 11)", v2, id, w, v2)
	}

	if id := lc.Root(v3); id != v1 {
		t.Errorf("Root(%v) = %v, want %v", v3, id, v1)
	}

	lc.Evert(v2)
	if id := lc.Root(v3); id != v2 {
		t.Errorf("Root(%v) = %v, want %v", v3, id, v2)
	}

	lc.Cut(v3)
	if id := lc.Root(v3); id != v3 {
		t.Errorf("Root(%v) = %v, want %v", v3, id, v3)
	}
}

func TestLinkCutSimpleLink(t *testing.T) {
	lc := NewLinkCut(comparator)
	v1 := lc.NewNode()
	v2 := lc.NewNode()
	lc.Root(v1)
	lc.Root(v2)
	v3 := lc.NewNode()
	lc.NewNode()
	v4 := lc.NewNode()

	lc.Link(v1, v3, 5)
	lc.Link(v3, v4, 1)
	lc.Link(v1, v2, 9)
	lc.Link(v3, v4, 7)
	lc.Root(v2)
	if id := lc.Root(v3); id != v2 {
		t.Errorf("Root(%v) = %v, want %v", v3, id, v2)
	}

}

func TestLinkCutSimpleCut(t *testing.T) {
	lc := NewLinkCut(comparator)
	v1 := lc.NewNode()
	v2 := lc.NewNode()
	v3 := lc.NewNode()
	v4 := lc.NewNode()

	lc.Link(v1, v4, 1)
	lc.Link(v1, v2, 1)
	lc.Link(v3, v4, 1)
	lc.Cut(v3)
	if id := lc.Root(v1); id != v2 {
		t.Errorf("Root(%v) = %v, want %v", v1, id, v2)
	}
}

func TestLinkCutSipleEvert(t *testing.T) {
	lc := NewLinkCut(comparator)
	v0 := lc.NewNode()
	v1 := lc.NewNode()
	v2 := lc.NewNode()
	lc.NewNode()
	v4 := lc.NewNode()

	lc.Link(v0, v4, 4)
	lc.Link(v2, v0, 9)
	lc.Link(v1, v2, 3)
	lc.Evert(v1)
	lc.Cut(v4)
	if id := lc.Root(v4); id != v4 {
		t.Errorf("Root(%v) = %v, want %v", v4, id, v4)
	}
}

func TestLinkCutRandom(t *testing.T) {
	n := 1000
	for i := 0; i < n; i++ {
		linkCutRandomTest(t)
	}
}

func linkCutRandomTest(t *testing.T) {
	n, m := 20, 100
	sLc := newSimpleLinkCut()
	lc := NewLinkCut(comparator)
	sIds := make([]NodeId, n)
	ids := make([]NodeId, n)
	sIdIdx := make(map[NodeId]int)
	idIdx := make(map[NodeId]int)
	for i := 0; i < n/2; i++ {
		sIdIdx[NodeId(i)] = i
		idIdx[NodeId(i)] = i
		sIds[i] = sLc.Root(NodeId(i))
		ids[i] = lc.Root(NodeId(i))
		if sIds[i] != NodeId(i) {
			t.Errorf("simple.Root(%v) = %v, want %v", i, sIds[i], i)
		}
		if ids[i] != NodeId(i) {
			t.Errorf("Root(%v) = %v, want %v", i, ids[i], i)
		}
	}
	for i := n / 2; i < n; i++ {
		sId := sLc.NewNode()
		id := lc.NewNode()
		sIds[i] = sId
		sIdIdx[sId] = i
		ids[i] = id
		idIdx[id] = i
	}
	for i := 0; i < m; i++ {
		u, v := rand.Intn(n), rand.Intn(n)
		weight := Weight(rand.Intn(10))
		switch rand.Intn(5) {
		case 0: // Root
			if sId, id := sLc.Root(sIds[v]), lc.Root(ids[v]); sIdIdx[sId] != idIdx[id] {
				t.Errorf("Root(%v) = %v, want %v", ids[v], id, ids[sIdIdx[sId]])
			}
		case 1: // Evert
			sLc.Evert(sIds[v])
			lc.Evert(ids[v])
		case 2: // Link
			sLc.Link(sIds[v], sIds[u], weight)
			lc.Link(ids[v], ids[u], weight)
		case 3: // Cut
			sLc.Cut(sIds[v])
			lc.Cut(ids[v])
		case 4: // MaxParentLink
			_, sW := sLc.MaxParentLink(sIds[v])
			id, w := lc.MaxParentLink(ids[v])
			if w != sLc.ParentWeight(sIds[idIdx[id]]) {
				t.Errorf("MaxParentLink(%v) = (%v, %v) - returened node does not have the specified parent link weight (it has %v).", ids[v], id, w, sLc.ParentWeight(sIds[idIdx[id]]))
			}
			if sW != w {
				t.Errorf("MaxParentLink(%v) = (%v, %v), want (..., %v)", ids[v], id, w, sW)
			}
		}
	}
}

func BenchmarkLinkCutTree(b *testing.B) {
	n := 100000
	lc := NewLinkCut(comparator)
	ids := make([]NodeId, n)
	for i := n; i < n; i++ {
		id := lc.NewNode()
		ids[i] = id
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u, v := rand.Intn(n), rand.Intn(n)
		weight := Weight(rand.Intn(10))
		switch rand.Intn(5) {
		case 0: // Root
			lc.Root(ids[v])
		case 1: // Evert
			lc.Evert(ids[v])
		case 2: // Link
			lc.Link(ids[v], ids[u], weight)
		case 3: // Cut
			lc.Cut(ids[v])
		case 4: // MaxParentLink
			lc.MaxParentLink(ids[v])
		}
	}
}
