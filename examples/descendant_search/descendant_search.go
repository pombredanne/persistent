// descendant_search package implements solution to descendant search problem.
package descendant_search

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"bitbucket.org/sapalskimichal/persistent/treemap"
)

// DescendantSearch represents solution to descendant search problem: preprocess a tree to allow quick answering for DescendantSearch queries.
type DescendantSearch interface {
	// DescendantSearch finds a descendant d of node a such that its key is closest possible to k among all descendants of a. A node is considered to be its own descendant for purpose of this problem.
	DescendantSearch(a, k int) int
}

type descendantSearch struct {
	lowestKey      int
	subtreeKeys    map[int]treemap.SummingMap
	lastOccurrence map[int]treemap.Map
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func (d descendantSearch) DescendantSearch(a, k int) int {
	keys := d.subtreeKeys[a]
	v, err := keys.Get(k)
	if err == nil && v.(int) > 0 {
		res, _ := d.lastOccurrence[a].Get(k)
		return res.(int)
	}

	sum := keys.Sum(d.lowestKey, k)
	high, errHigh := keys.FindByPrefixSum(sum + 1)
	low := high
	if sum > 0 {
		low, _ = keys.FindByPrefixSum(sum)
	}
	resKey := low.(int)
	if errHigh == nil && abs(low.(int)-k) > abs(high.(int)-k) {
		resKey = high.(int)
	}
	res, _ := d.lastOccurrence[a].Get(resKey)
	return res.(int)
}

// NewDescendantSearch constructs a solution to descendant search problem for a given tree.
func NewDescendantSearch(t tree.Tree) DescendantSearch {
	allKeys := map[int]int{}
	occurrencesMap := map[int]treemap.Value{}
	lowestKey := int((^uint(0)) >> 1)
	visit := func(n *tree.Node) {
		key := n.Data.(int)
		allKeys[key] = 0
		occurrencesMap[key] = -1
		if key < lowestKey {
			lowestKey = key
		}
	}
	t.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)

	subtreeKeys := map[int]treemap.SummingMap{}
	keys := treemap.NewStaticIntSummingMap(allKeys)

	lastOccurrence := map[int]treemap.Map{}
	occurrences := treemap.NewStaticIntMap(occurrencesMap)
	enter := func(n *tree.Node) {
		subtreeKeys[n.Id()] = keys
		key := n.Data.(int)
		keyCount, _ := keys.Get(key)
		keys, _ = keys.UpdateSumming(key, keyCount.(int)+1)
		occurrences, _ = occurrences.Update(key, n.Id())
	}
	exit := func(n *tree.Node) {
		subtreeKeys[n.Id()] = treemap.Subtract(keys, subtreeKeys[n.Id()])
		lastOccurrence[n.Id()] = occurrences
	}
	t.DFS(enter, tree.EmptyVisitor, exit)
	return descendantSearch{lowestKey, subtreeKeys, lastOccurrence}
}
