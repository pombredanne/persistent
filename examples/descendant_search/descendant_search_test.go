package descendant_search

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"math"
	"math/rand"
	"testing"
)

func child(id int, data int, childrenEdges ...*tree.Edge) *tree.Edge {
	return tree.NewChild(nil, id, data, childrenEdges...)
}

func TestDescendantSearch(t *testing.T) {
	n := tree.NewNode
	c := child
	tr := tree.Tree{
		n(0, 2,
			c(1, 1),
			c(2, 3,
				c(3, 2),
				c(4, 3,
					c(5, 1),
					c(6, 2))))}
	ds := NewDescendantSearch(tr)
	if res := ds.DescendantSearch(2, 1); res != 5 {
		t.Errorf("DescendantSearch(2, 1) = %v, want 5", res)
	}
	if res := ds.DescendantSearch(0, 4); res != 4 {
		t.Errorf("DescendantSearch(0, 4) = %v, want 4", res)
	}
	if res := ds.DescendantSearch(0, 0); res != 5 {
		t.Errorf("DescendantSearch(0, 0) = %v, want 5", res)
	}

	tr = tree.Tree{
		n(0, 5,
			c(1, 10,
				c(2, 8),
				c(3, 3,
					c(4, 7),
					c(5, 4)),
				c(6, 2,
					c(7, 2,
						c(8, 3)),
					c(9, 1))),
			c(10, 5,
				c(11, 3),
				c(12, 3,
					c(13, 2)),
				c(14, 8,
					c(15, 4),
					c(16, 1))))}

	ds = NewDescendantSearch(tr)
	if res := ds.DescendantSearch(1, 5); res != 5 {
		t.Errorf("DescendantSearch(1, 5) = %v, want 5", res)
	}
	if res := ds.DescendantSearch(10, 5); res != 10 {
		t.Errorf("DescendantSearch(10, 5) = %v, want 10", res)
	}
}

func _abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func randomDescendantSearch(n int) (DescendantSearch, map[int]int, tree.Tree) {
	depth := int(math.Sqrt(float64(n)) + 2)
	keys := map[int]int{}

	t := tree.RandomTree(n, depth)
	visit := func(node *tree.Node) {
		node.Data = rand.Intn(n)
		keys[node.Id()] = node.Data.(int)
	}
	t.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)
	return NewDescendantSearch(t), keys, t
}

func bruteForceDescendantSearch(t tree.Tree, keys map[int]int, a, k int) int {
	best := a
	inSubtree := false
	enter := func(node *tree.Node) {
		if node.Id() == a {
			inSubtree = true
		}
		if inSubtree {
			if _abs(keys[best]-k) > _abs(node.Data.(int)-k) {
				best = node.Id()
			}
		}
	}
	exit := func(node *tree.Node) {
		if node.Id() == a {
			inSubtree = false
		}
	}
	t.DFS(enter, tree.EmptyVisitor, exit)
	return best
}

func TestDescendandSearchBruteForce(t *testing.T) {
	n, m := 30, 90
	for i := 1; i <= n; i++ {
		ds, keys, tr := randomDescendantSearch(i)
		for j := 0; j < m; j++ {
			a := rand.Intn(i)
			k := rand.Intn(i)
			ans := bruteForceDescendantSearch(tr, keys, a, k)
			if res := ds.DescendantSearch(a, k); _abs(keys[res]-k) > _abs(keys[ans]-k) {
				t.Errorf("Test %v.%v: DescendantSearch(%v, %v) = %v, want %v (or equivalent)", i, j, a, k, res, ans)
			}
		}
	}
}

func BenchmarkDescendantSearch(b *testing.B) {
	n := 100000
	ds, _, _ := randomDescendantSearch(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, k := rand.Intn(n), rand.Intn(n)
		ds.DescendantSearch(a, k)
	}
}
