package tstring

import (
	"math/rand"
	"strings"
	"testing"
)

var runes = []string{"零", "一", "二", "三", "四", "五", "六", "七", "八", "九"}

func randomRune() rune {
	for _, r := range runes[rand.Intn(len(runes))] {
		return r
	}
	return '#'
}
func randomString(n int) TString {
	res := make([]string, n)
	for i := 0; i < n; i++ {
		res[i] = runes[rand.Intn(len(runes))]
	}
	return New(strings.Join(res, ""), P64)
}

func TestConversion(t *testing.T) {
	strs := []string{"abc", "xyzt", "Πάντα ῥεῖ", "覚", ""}
	for _, s := range strs {
		ts := New(s, P64)

		if sc := ts.String(); sc != s {
			t.Errorf("(%v).String() = %v, want %v", s, sc, s)
		}
	}
}

func runeAt(s string, k int64) rune {
	i := int64(0)
	for _, r := range s {
		if i == k {
			return r
		}
		i++
	}
	return '#'
}

func TestRuneAt(t *testing.T) {
	tests := []struct {
		s     string
		tests []int64
	}{
		{"abc", []int64{0, 1, 2}},
		{"Πάντα ῥεῖ", []int64{3, 4}},
		{"覚", []int64{0}},
	}
	for _, test := range tests {
		ts := New(test.s, P64)
		for _, index := range test.tests {
			cr := runeAt(test.s, index)
			if r, err := ts.RuneAt(index); err != nil || r != cr {
				t.Errorf("(%v).RuneAt(%v) = (%v, %v), want (%v, nil)", test.s, index, string(r), err, string(cr))
			}
		}
	}

	ts := New("foo", P64)
	if r, err := ts.RuneAt(3); err == nil {
		t.Errorf("(foo).RuneAt(3) = (%v, %v), want (*, error)", r, err)
	}
	if r, err := ts.RuneAt(-1); err == nil {
		t.Errorf("(foo).RuneAt(-1) = (%v, %v), want (*, error)", r, err)
	}
}

func BenchmarkRuneAt(b *testing.B) {
	n := 100000
	ts := randomString(n)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ts.RuneAt(int64(rand.Intn(n)))
	}
}

func TestLen(t *testing.T) {
	tests := []struct {
		s string
		l int64
	}{
		{"abc", 3},
		{"Πάντα ῥεῖ", 9},
		{"覚", 1},
		{"", 0},
	}
	for _, test := range tests {
		ts := New(test.s, P64)
		if l := ts.Len(); l != test.l {
			t.Errorf("(%v).Len() = %v, want %v", test.s, l, test.l)
		}
	}
}

func TestSetRuneAt(t *testing.T) {
	text := "Πάντα ῥεῖ"
	newRune := '覚'
	index := int64(5)
	newText := "Πάντα覚ῥεῖ"

	ts := New(text, P64)
	if nts, err := ts.SetRuneAt(index, newRune); err != nil || nts.String() != newText {
		t.Errorf("(%v).SetRuneAt(%v, %v) = (\"%v\", %v), want (\"%v\", nil)", text, index, newRune, nts.String(), err, newText)
	}
	if txt := ts.String(); txt != text {
		t.Errorf("(%v).SetRuneAt(%v, %v) modified the original text (is \"%v\", want \"%v\")", text, index, newRune, txt, text)
	}

	ts = New("foo", P64)
	if r, err := ts.SetRuneAt(3, 'x'); err == nil {
		t.Errorf("(foo).SetRuneAt(3, x) = (%v, %v), want (*, error)", r, err)
	}
	if r, err := ts.SetRuneAt(-1, 'y'); err == nil {
		t.Errorf("(foo).SetRuneAt(-1, y) = (%v, %v), want (*, error)", r, err)
	}
}

func BenchmarkSetRuneAt(b *testing.B) {
	n := 100000
	ts := randomString(n)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ts.SetRuneAt(int64(rand.Intn(n)), 'x')
	}
}

func split(s string, k int64) (string, string) {
	i := int64(0)
	for idx := range s {
		if k == i {
			return s[:idx], s[idx:]
		}
		i++
	}
	return s, ""
}

func TestSplit(t *testing.T) {
	tests := []struct {
		s       string
		indices []int64
	}{
		{"abc", []int64{0, 1, 2, 3}},
		{"Πάντα ῥεῖ", []int64{3, 4}},
		{"覚", []int64{0, 1}},
	}

	for _, test := range tests {
		ts := New(test.s, P64)
		for _, index := range test.indices {
			csa, csb := split(test.s, index)
			if sa, sb, err := ts.Split(index); err != nil || sa.String() != csa || sb.String() != csb {
				t.Errorf("(%v).Split(%v) = (\"%v\", \"%v\", %v), want (\"%v\", \"%v\", nil)", test.s, index, sa.String(), sb.String(), err, csa, csb)
			}
		}
	}

	ts := New("foo", P64)
	if a, b, err := ts.Split(4); err == nil {
		t.Errorf("(foo).Split(4) = (%v, %v, %v), want (*, *, error)", a, b, err)
	}
	if a, b, err := ts.Split(-1); err == nil {
		t.Errorf("(foo).Split(-1) = (%v, %v, %v), want (*, *, error)", a, b, err)
	}
}

func BenchmarkSplit(b *testing.B) {
	n := 100000
	ts := randomString(n)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		k := int64(rand.Intn(n + 1))
		ts.Split(k)
	}
}

func TestConcat(t *testing.T) {
	textA, textB := "Πάντα ῥεῖ ", "覚"
	tsA, tsB := New(textA, P64), New(textB, P64)

	if textAB := tsA.Concat(tsB).String(); textAB != textA+textB {
		t.Errorf("(%v).Concat(%v) = \"%v\", want \"%v\"", textA, textB, textAB, textA+textB)
	}
	if text := tsA.String(); text != textA {
		t.Errorf("(%v).Concat(%v) modified the original string (is \"%v\", want \"%v\")", textA, textB, text, textA)
	}
	if text := tsB.String(); text != textB {
		t.Errorf("(%v).Concat(%v) modified the original string (is \"%v\", want \"%v\")", textA, textB, text, textB)
	}
}

func BenchmarkConcat(b *testing.B) {
	n := 100000
	longA := randomString(n)
	longB := randomString(n)
	short := New("a", P64)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := longA, longB
		if rand.Intn(2) == 0 {
			b = short
		}
		if rand.Intn(2) == 0 {
			a, b = b, a
		}
		a.Concat(b)
	}
}

func TestEqual(t *testing.T) {
	textA, textB := New("foo", P64), New("foo", P64)
	if !textA.Equal(textB) {
		t.Errorf("(%v).Equal(%v) = false, want true", textA, textB)
	}
	if !textB.Equal(textA) {
		t.Errorf("(%v).Equal(%v) = false, want true", textB, textA)
	}

	textB = New("bar", P64)
	if textA.Equal(textB) {
		t.Errorf("(%v).Equal(%v) = true, want false", textA, textB)
	}
	if textB.Equal(textA) {
		t.Errorf("(%v).Equal(%v) = true, want false", textB, textA)
	}

	textB = New("foo!", P64)
	if textA.Equal(textB) {
		t.Errorf("(%v).Equal(%v) = true, want false", textA, textB)
	}
	if textB.Equal(textA) {
		t.Errorf("(%v).Equal(%v) = true, want false", textB, textA)
	}

	textA, textB = New("", P64), New("", P64)
	if !textA.Equal(textB) {
		t.Errorf("(%v).Equal(%v) = false, want true", textA, textB)
	}
	if !textB.Equal(textA) {
		t.Errorf("(%v).Equal(%v) = false, want true", textB, textA)
	}
}

func TestLess(t *testing.T) {
	textA, textB := New("foo", P64), New("foo", P64)
	if textA.Less(textB) {
		t.Errorf("(%v).Less(%v) = true, want false", textA, textB)
	}

	textB = New("bar", P64)
	if textA.Less(textB) {
		t.Errorf("(%v).Less(%v) = true, want false", textA, textB)
	}
	if !textB.Less(textA) {
		t.Errorf("(%v).Less(%v) = false, want true", textB, textA)
	}

	textB = New("fu", P64)
	if textB.Less(textA) {
		t.Errorf("(%v).Less(%v) = true, want false", textB, textA)
	}
	if !textA.Less(textB) {
		t.Errorf("(%v).Less(%v) = false, want true", textA, textB)
	}

}

func BenchmarkLessSmall(b *testing.B) {
	n, m := 100000, 10
	base := randomString(n)
	strs := make([]TString, 10)
	for i := 0; i < m; i++ {
		strs[i], _ = base.SetRuneAt(int64(rand.Intn(n)), randomRune())
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		strs[rand.Intn(m)].Less(strs[rand.Intn(m)])
	}
}

func BenchmarkLessLarge(b *testing.B) {
	m, k := 10, 40
	base1, base2 := New("a", P512), New("b", P512)
	for i := 0; i < k; i++ {
		b := base1
		if 0 == rand.Intn(2) {
			b = base2
		}
		base1 = base1.Concat(b)
		base2 = base2.Concat(b)
	}
	base := base1.Concat(base2)
	n := base.Len()
	strs := make([]TString, 10)
	for i := 0; i < m; i++ {
		strs[i], _ = base.SetRuneAt(rand.Int63n(n), randomRune())
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		strs[rand.Intn(m)].Less(strs[rand.Intn(m)])
	}
}
