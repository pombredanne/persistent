// level_ancestor package implements solution for level ancestor problem.
package level_ancestor

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
)

// LevelAncestor represents a solution to level ancestor problem: preprocess a tree to quickly answer level ancestor queries.
type LevelAncestor interface {
	// LevelAncestor returns the id of node that is an ancestor of node a and has depth k.
	LevelAncestor(a, k int) int
}

type levelAncestor struct {
	inTime map[int]int
	levels [][]int
}

func (l levelAncestor) LevelAncestor(a, k int) int {
	aInTime := l.inTime[a]
	low, high := 0, len(l.levels[k])
	for high-low > 1 {
		mid := (low + high) / 2
		if l.inTime[l.levels[k][mid]] > aInTime {
			high = mid
		} else {
			low = mid
		}
	}
	return l.levels[k][low]
}

// NewLevelAncestor returns a solution to level ancestor problem for the given tree.
func NewLevelAncestor(t tree.Tree) LevelAncestor {
	time := 0
	depth := -1
	inTime := map[int]int{}
	levels := make([][]int, t.Depth())
	enter := func(n *tree.Node) {
		inTime[n.Id()] = time
		time++
		depth++
		levels[depth] = append(levels[depth], n.Id())
	}
	exit := func(*tree.Node) {
		depth--
	}
	t.DFS(enter, tree.EmptyVisitor, exit)
	return levelAncestor{inTime, levels}
}
