package level_ancestor

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"math"
	"math/rand"
	"testing"
)

func randomLevelAncestor(n int) (LevelAncestor, map[int]int, tree.Tree) {
	depth := int(math.Sqrt(float64(n)) + 2)
	t := tree.RandomTree(n, depth)
	d := -1
	depths := map[int]int{}
	enter := func(n *tree.Node) {
		d++
		depths[n.Id()] = d
	}
	exit := func(*tree.Node) {
		d--
	}
	t.DFS(enter, tree.EmptyVisitor, exit)
	return NewLevelAncestor(t), depths, t
}

func bruteForceLevelAncestor(t tree.Tree, aDepth, a, k int) int {
	result := -1
	visitor := func(v, p *tree.Node) {
		if v.Id() == a {
			if aDepth == k {
				result = a
				return
			}
			aDepth--
			a = p.Id()
		}
	}
	t.ParentDFS(tree.EmptyParentVisitor, tree.EmptyParentVisitor, visitor)
	return result
}

func TestLevelAncestorBruteForce(t *testing.T) {
	n, m := 20, 60
	for i := 1; i <= n; i++ {
		ds, depths, tr := randomLevelAncestor(i)
		for j := 0; j < m; j++ {
			a := rand.Intn(i)
			k := rand.Intn(depths[a] + 1)
			ans := bruteForceLevelAncestor(tr, depths[a], a, k)
			if res := ds.LevelAncestor(a, k); res != ans {
				t.Errorf("Test %v.%v: LevelAncestor(%v, %v) = %v, want %v", i, j, a, k, res, ans)
			}
		}
	}
}

func BenchmarkLevelAncestor(b *testing.B) {
	n := 100000
	ds, depths, _ := randomLevelAncestor(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a := rand.Intn(n)
		k := rand.Intn(depths[a] + 1)
		ds.LevelAncestor(a, k)
	}
}
