// Package set implements solution for range set (size) query problem and path set size query problem.
package set

import (
	"bitbucket.org/sapalskimichal/persistent/examples/framework"
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"bitbucket.org/sapalskimichal/persistent/treemap"
)

// RangeSetSize represents solution to range set size problem: given an array, preprocess it to quickly answer SetSize queries.
type RangeSetSize interface {
	// SetSize returns how many different elements occurs in the range [a,b] of the underlying array.
	SetSize(a, b int) int
	// SetElements returns a slice containing all distinct elements that occur in range [a,b] of the underlying array. The elements will not be sorted.
	SetElements(a, b int) []string
	// KSetElements returns a slice containing at most k distinct elements that occur in range [a,b] of the underlying array. The elements will not be sorted.
	KSetElements(k, a, b int) []string
}

// PathSetElements represents solution to range set elements problem: given a tree, preprocess it to quickly answer SetElements queries.
type PathSetElements interface {
	// SetElements returns a slice containing all distinct elements that occur in on path between node a and node b in the underlying tree. The elements will not be sorted.
	SetElements(a, b int) []string
	// SetElements returns a slice containing at most k distinct elements that occur in on path between node a and node b in the underlying tree. The elements will not be sorted.
	KSetElements(k, a, b int) []string
}

type rangeSetSize struct {
	rangeQuery framework.RangeQuery
}

type pathSetElements struct {
	pathQuery framework.PathQuery
}

func (r *rangeSetSize) SetSize(a, b int) int {
	return r.rangeQuery.Query(a, b, sizeQuery).(int)
}

func (r *rangeSetSize) SetElements(a, b int) []string {
	return r.rangeQuery.Query(a, b, elementsQuery).([]string)
}

func (r *rangeSetSize) KSetElements(k, a, b int) []string {
	return r.rangeQuery.Query(a, b, elementsQuery, k).([]string)
}

func (p *pathSetElements) SetElements(a, b int) []string {
	return p.pathQuery.Query(a, b).([]string)
}

func (p *pathSetElements) KSetElements(k, a, b int) []string {
	return p.pathQuery.Query(a, b, k).([]string)
}

type sweep struct {
	position         int
	occurrences      treemap.Map
	isLastOccurrence treemap.SummingMap
}

type elementOccurrence struct {
	position int
	element  string
}

func at(idx int) elementOccurrence {
	return elementOccurrence{idx, ""}
}

func compare(eo1, eo2 treemap.Key) int {
	return eo1.(elementOccurrence).position - eo2.(elementOccurrence).position
}

const (
	sizeQuery     = true
	elementsQuery = false
)

func (s sweep) update(item string) framework.Sweep {
	newPosition := s.position + 1
	o, _ := s.occurrences.Get(item)
	lastOccurrence := o.(int)
	newIsLastOccurrence, _ := s.isLastOccurrence.UpdateSumming(elementOccurrence{lastOccurrence, item}, 0)
	newIsLastOccurrence, _ = newIsLastOccurrence.UpdateSumming(elementOccurrence{newPosition, item}, 1)
	newOccurrences, _ := s.occurrences.Update(item, newPosition)
	return sweep{newPosition, newOccurrences, newIsLastOccurrence}
}

// NewRangeSetSize constructs solution to range set size problem for the given array.
func NewRangeSetSize(items []string) RangeSetSize {
	elements := make([]framework.Element, 0, len(items))
	occurrencesMap := make(map[string]treemap.Value)
	isLastOccurrenceMap := make(map[treemap.Key]int)
	isLastOccurrenceMap[at(-1)] = 0
	n := len(items)
	for i, v := range items {
		elements = append(elements, v)
		occurrencesMap[v] = -1
		isLastOccurrenceMap[elementOccurrence{i, v}] = 0
	}

	initSweep := func([]framework.Element) framework.Sweep {
		return sweep{-1,
			treemap.NewStaticStringMap(occurrencesMap),
			treemap.NewStaticComparableSummingMap(compare, isLastOccurrenceMap)}
	}
	updateSweep := func(e framework.Element, s framework.Sweep) framework.Sweep {
		return s.(sweep).update(e.(string))
	}
	answerQuery := func(aSweep, bSweep framework.Sweep, args ...framework.Argument) framework.QueryResult {
		aPosition := aSweep.(sweep).position + 1
		if args[0].(bool) == sizeQuery {
			return bSweep.(sweep).isLastOccurrence.Sum(at(aPosition), at(n))
		} else {
			k := bSweep.(sweep).isLastOccurrence.Sum(at(aPosition), at(n))
			if len(args) == 2 && args[1].(int) < k {
				k = args[1].(int)
			}
			elements := bSweep.(sweep).isLastOccurrence.NonZeroEntries(at(aPosition), at(n), k)
			result := make([]string, 0, k)
			for _, e := range elements {
				result = append(result, e.(elementOccurrence).element)
			}
			return result
		}
	}
	return &rangeSetSize{framework.NewRangeQuery(elements, initSweep, updateSweep, answerQuery)}
}

// NewPathSetElements creates a new PathSetElements structure. The tree is not modified.
func NewPathSetElements(t tree.Tree) PathSetElements {
	occurrencesMap := make(map[string]treemap.Value)
	isLastOccurrenceMap := make(map[treemap.Key]int)
	isLastOccurrenceMap[at(-1)] = 0

	depth := -1
	maxDepth := -1

	enter := func(n *tree.Node) {
		depth++
		if depth > maxDepth {
			maxDepth = depth
		}
		p := n.ParentEdge
		if p != nil {
			s := p.Data.(string)
			occurrencesMap[s] = -1
			isLastOccurrenceMap[elementOccurrence{depth, s}] = 0
		}
	}
	exit := func(*tree.Node) {
		depth--
	}
	t.DFS(enter, tree.EmptyVisitor, exit)
	initSweep := func(tree.Tree) framework.Sweep {
		return sweep{0,
			treemap.NewStaticStringMap(occurrencesMap),
			treemap.NewStaticComparableSummingMap(compare, isLastOccurrenceMap)}
	}
	updateSweep := func(n *tree.Node, s framework.Sweep) framework.Sweep {
		return s.(sweep).update(n.ParentEdge.Data.(string))
	}
	answerQuery := func(a, b, lca framework.Sweep, args ...framework.Argument) framework.QueryResult {
		lcaPosition := lca.(sweep).position
		k := 2 * maxDepth
		if len(args) > 0 {
			k = args[0].(int)
		}
		aElements := a.(sweep).isLastOccurrence.NonZeroEntries(at(lcaPosition+1), at(maxDepth), k)
		bElements := b.(sweep).isLastOccurrence.NonZeroEntries(at(lcaPosition+1), at(maxDepth), k)

		set := make(map[string]bool)
		for _, e := range aElements {
			set[e.(elementOccurrence).element] = true
		}
		for _, e := range bElements {
			set[e.(elementOccurrence).element] = true
		}
		result := make([]string, 0, len(set))
		cnt := 0
		for e := range set {
			if cnt == k {
				break
			}
			result = append(result, e)
			cnt++
		}
		return result
	}

	return &pathSetElements{framework.NewPathQuery(t, initSweep, updateSweep, answerQuery)}
}
