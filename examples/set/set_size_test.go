package set

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"math"
	"math/rand"
	"strconv"
	"testing"
)

func randomValue(m int) string {
	return strconv.Itoa(rand.Intn(m))
}

func randomRangeSetSize(n, m int) (RangeSetSize, []string) {
	values := make([]string, 0, n)
	for i := 0; i < n; i++ {
		values = append(values, randomValue(m))
	}
	return NewRangeSetSize(values), values
}

func randomRange(n int) (a, b int) {
	a, b = rand.Intn(n), rand.Intn(n)
	if a > b {
		a, b = b, a
	}
	return
}

func TestRangeSetSize(t *testing.T) {
	r := NewRangeSetSize([]string{"c", "b", "d", "a", "c", "d", "c", "b", "e"})
	if result := r.SetSize(2, 5); result != 3 {
		t.Errorf("SetSize(2, 5) = %v, want 3", result)
	}

	if result := r.SetSize(4, 5); result != 2 {
		t.Errorf("SetSize(4, 5) = %v, want 2", result)
	}
}

func bruteForceRangeSetSize(values []string, a, b int) int {
	set := make(map[string]bool)
	for i := a; i <= b; i++ {
		set[values[i]] = true
	}
	return len(set)
}

func TestRangeSetSizeBruteForce(t *testing.T) {
	n, m := 20, 30
	for i := 1; i <= n; i++ {
		r, v := randomRangeSetSize(i, 1+int(math.Floor(math.Sqrt(float64(i)))))
		for j := 0; j < m; j++ {
			a, b := randomRange(i)
			ans := bruteForceRangeSetSize(v, a, b)
			if res := r.SetSize(a, b); res != ans {
				t.Errorf("Test %v.%v: %v SetSize(%v, %v) = %v, want %v", i, j, v, a, b, res, ans)
			}
		}
	}
}

func same(result, expected []string) bool {
	cntRes := make(map[string]int)
	cntExp := make(map[string]int)
	for _, r := range result {
		cntRes[r]++
	}
	for _, e := range expected {
		cntExp[e]++
	}
	if len(cntRes) != len(cntExp) {
		return false
	}
	for k, v := range cntRes {
		if cntExp[k] != v {
			return false
		}
	}
	return true
}

func subset(k int, result, expectedResult []string) bool {
	rElements := map[string]bool{}
	eElements := map[string]bool{}
	for _, e := range expectedResult {
		eElements[e] = true
	}
	for _, e := range result {
		if !eElements[e] {
			return false
		}
		rElements[e] = true
	}
	if len(rElements) != len(result) || len(result) > k {
		return false
	}
	return true
}

func TestRangeSetElements(t *testing.T) {
	r := NewRangeSetSize([]string{"c", "b", "d", "a", "c", "d", "c", "b", "e"})
	if result := r.SetElements(2, 5); !same(result, []string{"d", "a", "c"}) {
		t.Errorf("SetElements(2, 5) = %#v, want [\"d\", \"a\", \"c\"]", result)
	}

	if result := r.SetElements(4, 5); !same(result, []string{"c", "d"}) {
		t.Errorf("SetElements(4, 5) = %#v, want [\"c\", \"d\"]", result)
	}
}

func TestRangeKSetElements(t *testing.T) {
	r := NewRangeSetSize([]string{"c", "b", "d", "a", "c", "d", "c", "b", "e"})
	if result := r.KSetElements(2, 2, 5); !subset(2, result, []string{"d", "a", "c"}) {
		t.Errorf("KSetElements(2, 2, 5) = %#v, want subset of [\"d\", \"a\", \"c\"]", result)
	}

	if result := r.KSetElements(3, 4, 5); !same(result, []string{"c", "d"}) {
		t.Errorf("KSetElements(3, 4, 5) = %#v, want [\"c\", \"d\"]", result)
	}
}

func bruteForceRangeSetElements(values []string, a, b int) []string {
	set := make(map[string]bool)
	for i := a; i <= b; i++ {
		set[values[i]] = true
	}
	result := make([]string, 0, len(set))
	for k := range set {
		result = append(result, k)
	}
	return result
}

func TestRangeSetElementsBruteForce(t *testing.T) {
	n, m := 20, 30
	for i := 1; i <= n; i++ {
		r, v := randomRangeSetSize(i, 1+int(math.Floor(math.Sqrt(float64(i)))))
		for j := 0; j < m; j++ {
			a, b := randomRange(i)
			ans := bruteForceRangeSetElements(v, a, b)
			if res := r.SetElements(a, b); !same(res, ans) {
				t.Errorf("Test %v.%v: %v SetElements(%v, %v) = %#v, want %#v", i, j, v, a, b, res, ans)
			}
			k := rand.Intn(len(ans) + 4)
			if res := r.KSetElements(k, a, b); !subset(k, res, ans) {
				t.Errorf("Test %v.%v: %v KSetElements(%v, %v, %v) = %#v, want subset of %#v", i, j, v, k, a, b, res, ans)
			}
		}
	}
}

func BenchmarkRangeSetSize(b *testing.B) {
	n, m := 100000, 1000
	ds, _ := randomRangeSetSize(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		ds.SetSize(a, b)
	}
}

func BenchmarkRangeSetElements(b *testing.B) {
	n, m := 100000, 1000
	ds, _ := randomRangeSetSize(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		ds.SetElements(a, b)
	}
}

func BenchmarkRangeKSetElements(b *testing.B) {
	n, m := 100000, 1000
	ds, _ := randomRangeSetSize(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		k := rand.Intn(10)
		ds.KSetElements(k, a, b)
	}
}

func randomPathSetElements(n int, m int) (PathSetElements, tree.Tree) {
	depth := int(math.Sqrt(float64(n)) + 2)
	t := tree.RandomTree(n, depth)
	visit := func(node *tree.Node) {
		p := node.ParentEdge
		if p != nil {
			p.Data = randomValue(m)
		}
	}
	t.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)
	return NewPathSetElements(t), t
}

func bruteForcePathSetElements(t tree.Tree, a, b int) []string {
	set := make(map[string]bool)
	visitor := func(v, p *tree.Node) {
		if v.Id() == a || v.Id() == b {
			if v.Id() == a && v.Id() == b {
				a = -1
				b = -1
			} else {
				set[v.ParentEdge.Data.(string)] = true
			}
		}
		if v.Id() == a {
			a = p.Id()
		}
		if v.Id() == b {
			b = p.Id()
		}
	}
	t.ParentDFS(tree.EmptyParentVisitor, tree.EmptyParentVisitor, visitor)
	result := make([]string, 0, len(set))
	for k := range set {
		result = append(result, k)
	}
	return result
}

func TestPathSetElementBruteForce(t *testing.T) {
	n, m := 20, 30
	for i := 1; i <= n; i++ {
		ds, tr := randomPathSetElements(i, 1+int(math.Floor(math.Sqrt(float64(i)))))
		for j := 0; j < m; j++ {
			a, b := rand.Intn(i), rand.Intn(i)
			ans := bruteForcePathSetElements(tr, a, b)
			if res := ds.SetElements(a, b); !same(res, ans) {
				t.Errorf("Test %v.%v: SetElements(%v, %v) = %#v, want %#v", i, j, a, b, res, ans)
			}
			k := rand.Intn(len(ans) + 4)
			if res := ds.KSetElements(k, a, b); !subset(k, res, ans) {
				t.Errorf("Test %v.%v: KSetElements(%v, %v, %v) = %#v, want subset of %#v", i, j, k, a, b, res, ans)
			}
		}
	}
}

func BenchmarkPathSetElements(b *testing.B) {
	n, m := 100000, 100
	ds, _ := randomPathSetElements(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		ds.SetElements(a, b)
	}
}

func BenchmarkPathKSetElements(b *testing.B) {
	n, m := 100000, 100
	ds, _ := randomPathSetElements(n, m)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		k := rand.Intn(10)
		ds.KSetElements(k, a, b)
	}
}
