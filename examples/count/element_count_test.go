package count

import (
	"bitbucket.org/sapalskimichal/persistent/examples/tree"
	"math"
	"math/rand"
	"strconv"
	"testing"
)

func randomRangeElementCount(n int) (RangeElementCount, []string) {
	values := make([]string, 0, n)
	for i := 0; i < n; i++ {
		values = append(values, randomElement(n))
	}
	return NewRangeElementCount(values), values
}

func randomRange(n int) (a, b int) {
	a, b = rand.Intn(n), rand.Intn(n)
	if a > b {
		a, b = b, a
	}
	return
}

func TestRangeElementCountQuery(t *testing.T) {
	r := NewRangeElementCount([]string{"c", "a", "d", "c", "a", "d", "c", "a", "d"})

	if result := r.ElementCount("c", 2, 6); result != 2 {
		t.Errorf("ElementCount(\"c\", 2, 6) = %v, want 2", result)
	}

	if result := r.ElementCount("z", 1, 8); result != 0 {
		t.Errorf("ElementCount(\"z\", 1, 8) = %v, want 0", result)
	}

	if result := r.IntervalCount("b", "d", 2, 7); result != 4 {
		t.Errorf("IntervalCount(\"b\", \"d\", 2, 7) = %v, want 4", result)
	}
	if result := r.IntervalCount("d", "b", 2, 7); result != 0 {
		t.Errorf("IntervalCount(\"d\", \"b\", 2, 7) = %v, want 0", result)
	}
}

func BenchmarkRangeElementCount(b *testing.B) {
	n := 100000
	ds, _ := randomRangeElementCount(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		ds.ElementCount(randomElement(n), a, b)
	}
}

func bruteForceRangeIntervalCount(values []string, x, y string, a, b int) int {
	result := 0
	for i := a; i <= b; i++ {
		if x <= values[i] && values[i] <= y {
			result++
		}
	}
	return result
}

func randomElement(n int) string {
	return strconv.Itoa(rand.Intn(n))
}

func TestRangeElementCountBruteForce(t *testing.T) {
	n, m := 30, 40
	for i := 1; i <= n; i++ {
		ds, v := randomRangeElementCount(i)
		for j := 0; j < m; j++ {
			a, b := randomRange(i)
			x, y := randomElement(i), randomElement(i)
			elementCount := bruteForceRangeIntervalCount(v, x, x, a, b)
			if count := ds.ElementCount(x, a, b); count != elementCount {
				t.Errorf("(%v).ElementCount(%#v, %v, %v) = %v, want %v", v, x, a, b, count, elementCount)
			}

			intervalCount := bruteForceRangeIntervalCount(v, x, y, a, b)
			if count := ds.IntervalCount(x, y, a, b); count != intervalCount {
				t.Errorf("(%v).IntervalCount(%#v, %#v, %v, %v) = %v, want %v", v, x, y, a, b, count, intervalCount)
			}
		}
	}
}

func BenchmarkRangeIntervalCount(b *testing.B) {
	n := 100000
	ds, _ := randomRangeElementCount(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := randomRange(n)
		ds.IntervalCount(randomElement(n), randomElement(n), a, b)
	}
}

func TestPathElementCountQuery(t *testing.T) {
	n := tree.NewNode
	c := tree.NewChild
	tree := tree.Tree{n(1, "A", c("b", 2, "B", c("a", 3, "A"), c("c", 4, "C")), c("b", 5, "B"), c("c", 6, "C"))}
	ds := NewPathElementCount(tree)
	tests := []struct {
		element      string
		a, b, result int
	}{
		{"a", 1, 1, 0},
		{"b", 4, 3, 0},
		{"b", 2, 5, 2},
		{"z", 1, 6, 0},
	}

	for i, test := range tests {
		if result := ds.ElementCount(test.element, test.a, test.b); result != test.result {
			t.Errorf("Test %v: ElementCount(%v, %v, %v) = %v, want %v", i, test.element, test.a, test.b, result, test.result)
		}
	}

	if result := ds.IntervalCount("b", "c", 4, 5); result != 3 {
		t.Errorf("IntervalCount(\"b\", \"c\", 4, 5) = %v, want 3", result)
	}
}

func randomPathElementCount(n int) (PathElementCount, tree.Tree) {
	depth := int(math.Sqrt(float64(n)) + 2)
	t := tree.RandomTree(n, depth)
	visit := func(node *tree.Node) {
		if node.ParentEdge != nil {
			node.ParentEdge.Data = randomElement(n)
		}
	}
	t.DFS(visit, tree.EmptyVisitor, tree.EmptyVisitor)
	return NewPathElementCount(t), t
}

func BenchmarkPathElementCount(b *testing.B) {
	n := 100000
	ds, _ := randomPathElementCount(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		ds.ElementCount(randomElement(n), a, b)
	}
}

func BenchmarkPathIntervalCount(b *testing.B) {
	n := 100000
	ds, _ := randomPathElementCount(n)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		a, b := rand.Intn(n), rand.Intn(n)
		ds.IntervalCount(randomElement(n), randomElement(n), a, b)
	}
}

func bruteForcePathIntervalCount(t tree.Tree, x, y string, a, b int) int {
	result := 0
	visitor := func(v, p *tree.Node) {
		if v.Id() == a || v.Id() == b {
			if v.Id() == a && v.Id() == b {
				a = -1
				b = -1
			} else {
				s := v.ParentEdge.Data.(string)
				if x <= s && s <= y {
					result++
				}
			}
		}
		if v.Id() == a {
			a = p.Id()
		}
		if v.Id() == b {
			b = p.Id()
		}
	}
	t.ParentDFS(tree.EmptyParentVisitor, tree.EmptyParentVisitor, visitor)
	return result
}

func TestPathElementCountBruteForce(t *testing.T) {
	n, m := 20, 60
	for i := 1; i <= n; i++ {
		ds, tr := randomPathElementCount(i)
		for j := 0; j < m; j++ {
			a, b := rand.Intn(i), rand.Intn(i)
			x, y := randomElement(i), randomElement(i)
			ans := bruteForcePathIntervalCount(tr, x, x, a, b)
			if res := ds.ElementCount(x, a, b); res != ans {
				t.Errorf("Test %v.%v: ElementCount(%#v, %v, %v) = %v, want %v", i, j, x, a, b, res, ans)
			}
			ans = bruteForcePathIntervalCount(tr, x, y, a, b)
			if res := ds.IntervalCount(x, y, a, b); res != ans {
				t.Errorf("%v", tr.GraphvizString())
				t.Errorf("Test %v.%v: IntervalCount(%#v, %#v, %v, %v) = %v, want %v", i, j, x, y, a, b, res, ans)
			}
		}
	}
}
