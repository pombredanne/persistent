# README #

This repository stores source code accompanying my Msc. thesis "Algorithmic Applications of Persistent Data Structures".
It contains implementation of persistent binary search tree and a few applications, see [the paper](https://michalsapalski.student.tcs.uj.edu.pl/Algorithmic%20Applications%20of%20Persistent%20Data%20Structures.pdf) for more detailed description.

The code is released on BSD license (see the LICENSE file).